<?php

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
$modName = 'food_cat';

#handle actions here.
switch ($action):
    # Get Food Cat List
    case'list':
        $QueryObj = new foodCat;
        $record = $QueryObj->listFoodCat();
        break;

    # Add Food Cat
    case'insert':
        $QueryObj = new foodCat;
        $top_cats = $QueryObj->getTopCats();
        if (isset($_POST['submit'])):
            $QueryObj = new foodCat;
            $QueryObj->saveFoodCat($_POST);
            $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
            Redirect(make_admin_url('food_cat', 'list', 'list'));
        endif;
        break;

    # Update Food
    case'update':
        $QueryObj = new foodCat;
        $food = $QueryObj->getFoodCat($id);
//        $QueryObj = new foodCat;
        $top_cats = $QueryObj->getTopCats($id);
        # Update Food
        if (isset($_POST['submit'])):
            $QueryObj = new foodCat;
            $QueryObj->saveFoodCat($_POST);
            $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
            Redirect(make_admin_url('food_cat', 'list', 'list'));
        endif;
        break;
    # Delete foodCat
    case'delete':
        $QueryObj = new foodCat();
        $QueryObj->deleteFoodCat($id);
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('food_cat', 'list', 'list'));
        break;
    default:break;
endswitch;
?>