<?php

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';


$modName = 'food';

#Food Category List
$QueryObj = new foodCat;
$foodCatList = $QueryObj->getTopCats(1);

#handle actions here.
switch ($action):
    # Get Food List
    case'list':
        $QueryObj = new food;
        $record = $QueryObj->listFood();
        break;

    # Add Food
    case'insert':
        if (isset($_POST['submit'])):
            $QueryObj = new food;
            $food_id = $QueryObj->saveFood($_POST);

            #save Cat Rel
            if (isset($_POST['cat']) && !empty($_POST['cat'])):
                $QueryObj = new foodCatRel;
                $QueryObj->saveFoodCatRel($_POST['cat'], $food_id);
            endif;
            ;

            $admin_user->set_pass_msg(MSG_ADD_FOOD_SUCCESS);
            Redirect(make_admin_url('food', 'list', 'list'));
        endif;
        break;

    # Update Food 
    case'update':
        $QueryObj = new food;
        $food = $QueryObj->getFood($id);

        #food Category Rel
        $QueryObj = new foodCatRel;
        $catRel = $QueryObj->getFoodCatRelInArray($id);

        # Update Food
        if (isset($_POST['submit'])):
            $QueryObj = new food;
            $food_id = $QueryObj->saveFood($_POST);

            #save Cat Rel
            if (isset($_POST['cat']) && !empty($_POST['cat'])):
                #delete Old Rel
                $QueryObj = new foodCatRel;
                $QueryObj->deleteOldRel($food_id);

                #save New Rel
                $QueryObj = new foodCatRel;
                $QueryObj->saveFoodCatRel($_POST['cat'], $food_id);
            endif;
            $admin_user->set_pass_msg(MSG_EDIT_FOOD_SUCCESS);
            Redirect(make_admin_url('food', 'list', 'list'));
        endif;
        break;

    # Get Food Trash List	
    case'thrash':
        $QueryObj = new food;
        $record = $QueryObj->listTrashFood();
        break;

    # Delete Food 
    case'delete':
        $QueryObj = new food();
        $QueryObj->deleteFood($id);
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('food', 'list', 'list'));
        break;

    # Restore Food 
    case'restore':
        $QueryObj = new food();
        $QueryObj->restoreObject($id);
        $admin_user->set_pass_msg(MSG_RESTORE_FOOD_SUCCESS);
        Redirect(make_admin_url('food', 'thrash', 'thrash'));
        break;

    # Delete Premanent Food 	
    case'del_per':
        $QueryObj = new food();
        $QueryObj->purgeObject($id);
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('food', 'thrash', 'thrash'));
        break;
    default:break;
endswitch;
?>
