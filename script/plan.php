<?php

include_once DIR_FS_SITE_INCLUDE_FUNCTION_CLASS . 'planClass.php';
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_REQUEST['step']) ? $step = $_REQUEST['step'] : $step = '1';
$plan_id = isset($_GET['plan_id']) && intval($_GET['plan_id']) > 0 ? intval($_GET['plan_id']) : 0;
$modName = 'plan';
if ($plan_id) {
    $obj = new plan_info;
    $plan_info = $obj->getPlanInfo($plan_id);
    $QueryObj = new user;
    $user_info = $QueryObj->getUser($plan_info[0]->user_id);
}
#All User Information Limited
$QueryObj = new user;
$allUser = $QueryObj->limitedUserInfo();

#All Activity Factor Information Limited
$QueryObj = new activityFactor;
$allFactor = $QueryObj->listRecords(1);

#All Method List
$QueryObj = new methodList;
$methodList = $QueryObj->listRecords(1);
$calorie_profiles = array(
    'weight_loss' => array(
        'label' => 'Weight Loss',
        'default' => -20
    ),
    'maintenance' => array(
        'label' => 'Maintenance',
        'default' => 0
    ),
    'muscle_gain' => array(
        'label' => 'Muscle Gain',
        'default' => 10
    ),
);

#handle actions here.
switch ($action) {
    case 'list':
        if ($section == 'report') {
            $obj = new query('plan as p,plan_info as pi');
            $obj->Where = "WHERE p.id=pi.plan_id AND p.id=$id";
            $plan_info = $obj->ListOfAllRecords('object');
            foreach ($plan_info as $plan) {
                $plan->protein_per_day = $plan->protein_per_day * 4;
                if ($plan->plan_type === 'training') {
                    $t_plan = $plan;
                    $t_plan->day_carbs_fats = abs($t_plan->target_calories - $t_plan->protein_per_day);
                    $t_plan->meal_carbs_fats = round($t_plan->day_carbs_fats / $t_plan->total_meals, 2);
                    $t_plan->meal_carbs = round($t_plan->meal_carbs_fats * ($t_plan->carbs / 100), 2);
                    $t_plan->meal_fats = $t_plan->meal_carbs_fats - $t_plan->meal_carbs;
                    $t_plan->meal_proteins = round($t_plan->protein_per_day / $t_plan->total_meals, 2);
//                    pr($t_plan);die;
                    $obj = new plan_meals;
                    $obj->Where = "WHERE plan_info_id='$plan->id'";
                    $data = $obj->ListOfAllRecords('object');
                    $t_plan_meals = $data;
                }
                if ($plan->plan_type === 'non_training') {
                    $nt_plan = $plan;
                    $nt_plan->day_carbs_fats = abs($nt_plan->target_calories - $nt_plan->protein_per_day);
                    $nt_plan->meal_carbs_fats = round($nt_plan->day_carbs_fats / $nt_plan->total_meals, 2);
                    $nt_plan->meal_carbs = round($nt_plan->meal_carbs_fats * ($t_plan->carbs / 100), 2);
                    $nt_plan->meal_fats = $nt_plan->meal_carbs_fats - $nt_plan->meal_carbs;
                    $nt_plan->meal_proteins = round($nt_plan->protein_per_day / $nt_plan->total_meals, 2);
                    $obj = new plan_meals;
                    $obj->Where = "WHERE plan_info_id='$plan->id'";
                    $data = $obj->ListOfAllRecords('object');
                    $nt_plan_meals = $data;
                }
            }
            $obj = new user;
            $user = $obj->getUser($t_plan->user_id);
            $obj = new food;
            $obj->Where = "WHERE is_active='1'";
            $foods = $obj->ListOfAllRecords('object');
            $food_types = array(
                'protein',
                'fat',
                'carb',
            );
            break;
        }
        $obj = new plan;
        $record = $obj->ListOfAllRecords('object');
//        pr($record);die;
        break;
    case 'insert':
        if (isset($_POST['submit']) || isset($_POST['submit_t']) || isset($_POST['submit_nt'])) {
//            pr($_POST, TRUE);
//            die;
            $admin_user_id = $admin_user->get_user_id();
            $user_id = $_POST['user_id'];
            $data = array(
                'user_id' => $user_id,
                'created_by' => $admin_user_id,
                'is_active' => 1,
            );
            $QueryObj = new user;
            $user = $QueryObj->getUser($user_id);
            $obj = new plan();
            $plan_id = $obj->savePlan($data);
//            $_SESSION['nutrition_plan_id'] = $plan_id;
//            echo $plan_id;
            $data = array(
                'user_id' => $user_id,
                'plan_id' => $plan_id,
                'weight_type' => $_POST['weight_type'],
                'weight' => $_POST['weight'],
                'height' => $_POST['height'],
                'fat' => $_POST['fat'],
                'bmr' => $_POST['bmr'],
                'age' => show_age($user->date_of_birth, TRUE),
                // ---------------------------------------- //
                'activity_factor' => $_POST['activity_factor'],
                'method' => $_POST['method'],
                'training_per_week' => $_POST['training_per_week'],
                'training_time' => $_POST['training_time'],
                'total_meals' => $_POST['total_meals'],
                'cardio_per_week' => $_POST['cardio_per_week'],
                'wake_up_time' => $_POST['wake_up_time'],
                'sleep_time' => $_POST['sleep_time'],
                'fats' => $_POST['fats'],
                'carbs' => $_POST['carbs'],
                'protein_per_day' => $_POST['protein_per_day'],
                'target_calories' => $_POST['target_calories'],
                'day_calories' => $_POST['day_calories'],
                //-----------------
                'ppkg' => $_POST['ppkg'],
                'calorie_profile' => $_POST['calorie_profile'],
                'calorie_profile_percent' => $_POST['calorie_profile_percent'],
                'differential' => $_POST['differential'],
            );
            $obj = new plan_info;
            $obj->savePlanInfo($data);
            $data['plan_type'] = 'non_training';
//            $data['activity_factor'] = $_POST['nt_activity_factor'];
//            $data['method'] = $_POST['nt_method'];
            $data['training_per_week'] = 0;
            $data['training_time'] = '';
//            $data['total_meals'] = $_POST['nt_total_meals'];
            $data['cardio_per_week'] = 0;
//            $data['wake_up_time'] = $_POST['nt_wake_up_time'];
//            $data['sleep_time'] = $_POST['nt_sleep_time'];
            //-------------------------------------
//            $data['fats'] = $_POST['fats'] * (1 - ((100 - $_POST['differential']) / 100));
//            $data['carbs'] = $_POST['carbs'] * (1 - ((100 - $_POST['differential']) / 100));
            $data['protein_per_day'] = $_POST['protein_per_day'] * (1 - ((100 - $_POST['differential']) / 100));
            $data['target_calories'] = $_POST['target_calories'] * (1 - ((100 - $_POST['differential']) / 100));
            $data['day_calories'] = $_POST['day_calories'] * (1 - ((100 - $_POST['differential']) / 100));
            //-------------------------------------
            $obj = new plan_info;
            $obj->savePlanInfo($data);
            $admin_user->set_pass_msg("Plan created successfully");
            if (isset($_POST['submit_t'])) {
                Redirect(make_admin_url('plan', 'update', 't_plan', 'id=' . $plan_id));
            } elseif (isset($_POST['submit_nt'])) {
                Redirect(make_admin_url('plan', 'update', 'nt_plan', 'id=' . $plan_id));
            } else {
                Redirect(make_admin_url('plan', 'list', 'list'));
            }
        }
        break;
    case 'delete':
        $obj = new plan;
        $obj->id = $id;
        $obj->Delete();
        $admin_user->set_pass_msg('Plan has been deleted successfully');
        Redirect(make_admin_url('plan', 'list', 'list'));
        break;
    case 'update':
        if (!$id) {
            $admin_user->set_error();
            $admin_user->set_pass_msg("Something went wrong, Please try again..!!");
            Redirect(make_admin_url('plan'));
        }
        if (!plan::valid($id)) {
            $admin_user->set_error();
            $admin_user->set_pass_msg("Invalid Plan, Please try again..!!");
            Redirect(make_admin_url('plan'));
        }
        if ($section == 't_plan' || $section == 'nt_plan') {
            $obj = new query('plan as p,plan_info as pi');
            $obj->Where = "WHERE p.id=pi.plan_id AND p.id=$id";
            $plan_info = $obj->ListOfAllRecords('object');
            foreach ($plan_info as $plan) {
                $plan->protein_per_day = $plan->protein_per_day * 4;
                $obj = new plan_meals;
                $obj->Where = "WHERE plan_info_id='$plan->id'";
                $data = $obj->ListOfAllRecords('object');
                if ($plan->plan_type === 'training') {
                    $t_plan = $plan;
                    $t_plan->day_carbs_fats = abs($t_plan->target_calories - $t_plan->protein_per_day);
                    $t_plan->meal_carbs_fats = round($t_plan->day_carbs_fats / $t_plan->total_meals);
                    $t_plan->meal_carbs = round($t_plan->meal_carbs_fats * ($t_plan->carbs / 100));
                    $t_plan->meal_fats = $t_plan->meal_carbs_fats - $t_plan->meal_carbs;
                    $t_plan->meal_proteins = round($t_plan->protein_per_day / $t_plan->total_meals);
                    $t_plan->meal_calories = $t_plan->meal_carbs_fats + $t_plan->meal_proteins;
                    $t_plan_meals = $data;
                }
                if ($plan->plan_type === 'non_training') {
                    $nt_plan = $plan;
                    $nt_plan->day_carbs_fats = abs($nt_plan->target_calories - $nt_plan->protein_per_day);
                    $nt_plan->meal_carbs_fats = round($nt_plan->day_carbs_fats / $nt_plan->total_meals);
                    $nt_plan->meal_carbs = round($nt_plan->meal_carbs_fats * ($t_plan->carbs / 100));
                    $nt_plan->meal_fats = $nt_plan->meal_carbs_fats - $nt_plan->meal_carbs;
                    $nt_plan->meal_proteins = round($nt_plan->protein_per_day / $nt_plan->total_meals);
                    $nt_plan->meal_calories = $nt_plan->meal_carbs_fats + $nt_plan->meal_proteins;
                    $nt_plan_meals = $data;
                }
            }
            $obj = new user;
            $user = $obj->getUser($t_plan->user_id);
            $obj = new food;
            $obj->Where = "WHERE is_active='1'";
            $foods = $obj->ListOfAllRecords('object');
            $food_types = array(
                'protein',
                'fat',
                'carb',
            );
            foreach ($food_types as $food_type) {
                $obj = new food;
                $obj->Where = "WHERE is_active='1' AND $food_type > 0 order by $food_type desc";
                ${$food_type . '_foods'} = $obj->ListOfAllRecords('object');
            }
            if (isset($_POST['submit'])) {
                $tot_meals = $section == 't_plan' ? $t_plan->total_meals : $nt_plan->total_meals;
                for ($i = 1; $i <= $tot_meals; $i++) {
                    $meal = array();
                    if (isset($_POST['plan_meal_id'])) {
                        $meal['id'] = $_POST['plan_meal_id'][$i];
                    }
                    $meal['plan_id'] = $id;
                    $meal['plan_info_id'] = $_POST['plan_info_id'];
                    $meal['meal_name'] = $_POST['meal_name'][$i];
                    $meal['meal_category'] = $_POST['meal_category'][$i];
                    $meal['meal_proteins'] = $_POST['meal_proteins'][$i];
                    $meal['meal_carbs'] = $_POST['meal_carbs'][$i];
                    $meal['meal_fats'] = $_POST['meal_fats'][$i];
                    $meal['meal_calories'] = $_POST['meal_calories'][$i];
                    $meal['meal_time'] = $_POST['meal_time'][$i];
                    $meal['meal_comments'] = $_POST['meal_comments'][$i];
                    $obj = new plan_meals;
                    $meal_id = $obj->savePlanMeal($meal);
                    $obj = new meal_foods;
                    $obj->Where = "WHERE meal_id=$meal_id";
                    $obj->Delete_where();
                    $post_meal_foods = $_POST['meal_foods'][$i];
                    for ($j = 0; $j < count($post_meal_foods['food_id']); $j++) {
                        if (!$post_meal_foods['food_id'][$j])
                            continue;
                        $meal_food = array(
                            'meal_id' => $meal_id,
                            'food_id' => $post_meal_foods['food_id'][$j],
                            'quantity' => $post_meal_foods['quantity'][$j],
                        );
                        $obj = new meal_foods;
                        $obj->saveMealFood($meal_food);
                    }
                }
                $admin_user->set_pass_msg("Meal Information Updated Successfully!");
                Redirect(make_admin_url($Page, $action, $section, 'id=' . $id));
            }
            break;
        }
        if (isset($_POST['submit']) || isset($_POST['submit_t']) || isset($_POST['submit_nt'])) {
            $data = array(
                'id' => $_POST['t_plan_id'],
                'weight' => $_POST['weight'],
                'weight_type' => $_POST['weight_type'],
                'height' => $_POST['height'],
                'fat' => $_POST['fat'],
                'bmr' => $_POST['bmr'],
                // ---------------------------------------- //
                'activity_factor' => $_POST['activity_factor'],
                'method' => $_POST['method'],
                'training_per_week' => $_POST['training_per_week'],
                'training_time' => $_POST['training_time'],
                'total_meals' => $_POST['total_meals'],
                'cardio_per_week' => $_POST['cardio_per_week'],
                'wake_up_time' => $_POST['wake_up_time'],
                'sleep_time' => $_POST['sleep_time'],
                'fats' => $_POST['fats'],
                'carbs' => $_POST['carbs'],
                'protein_per_day' => $_POST['protein_per_day'],
                'target_calories' => $_POST['target_calories'],
                'day_calories' => $_POST['day_calories'],
                //---------------------------------------
                'ppkg' => $_POST['ppkg'],
                'calorie_profile' => $_POST['calorie_profile'],
                'calorie_profile_percent' => $_POST['calorie_profile_percent'],
                'differential' => $_POST['differential'],
            );
            $obj = new plan_info;
            $obj->savePlanInfo($data);
            // ------ For Non-Training ------ //
            $data['id'] = $_POST['nt_plan_id'];
//            $data['activity_factor'] = $_POST['nt_activity_factor'];
//            $data['method'] = $_POST['nt_method'];
//            $data['total_meals'] = $_POST['nt_total_meals'];
//            $data['wake_up_time'] = $_POST['nt_wake_up_time'];
//            $data['sleep_time'] = $_POST['nt_sleep_time'];
//            $data['fats'] = $_POST['nt_fats'];
//            $data['carbs'] = $_POST['nt_carbs'];
            $data['protein_per_day'] = $_POST['protein_per_day'] * (1 - ((100 - $_POST['differential']) / 100));
            $data['target_calories'] = $_POST['target_calories'] * (1 - ((100 - $_POST['differential']) / 100));
            $data['day_calories'] = $_POST['day_calories'] * (1 - ((100 - $_POST['differential']) / 100));
            $obj = new plan_info;
            $obj->savePlanInfo($data);
            $admin_user->set_pass_msg("Information Updated Successfully");
            if (isset($_POST['submit_t'])) {
                Redirect(make_admin_url('plan', 'update', 't_plan', 'id=' . $id));
            } elseif (isset($_POST['submit_nt'])) {
                Redirect(make_admin_url('plan', 'update', 'nt_plan', 'id=' . $id));
            } else {
                Redirect(make_admin_url('plan', 'list', 'list'));
            }
        }
        $obj = new query('plan as p,plan_info as pi');
        $obj->Where = "WHERE p.id=pi.plan_id AND p.id=$id";
        $plan_info = $obj->ListOfAllRecords('object');
        foreach ($plan_info as $plan) {
            if ($plan->plan_type === 'training') {
                $t_plan = $plan;
            }
            if ($plan->plan_type === 'non_training') {
                $nt_plan = $plan;
            }
        }
        $obj = new user;
        $user = $obj->getUser($t_plan->user_id);
        break;
}
?>