<?
isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';


$modName='user';
#handle actions here.
switch ($action):
	# Get User List
	case'list':
			$QueryObj=new user();
			$record=$QueryObj->listUsers(); 			
	break;
	
	# Add user
	case'insert':
		if(isset($_POST['submit'])):
			$QueryObj=new user();
			$check=$QueryObj->checkField('email',$_POST['email']);	
			
			if(is_object($check)):
				$admin_user->set_pass_msg('Email Already Exist');
				Redirect(make_admin_url('user', 'insert', 'insert'));				
			endif;
			$QueryObj=new user();
			$QueryObj->saveUser($_POST); 
			$admin_user->set_pass_msg(MSG_ADD_USER_SUCCESS);
			Redirect(make_admin_url('user', 'list', 'list'));				
		endif;	
	break;
	
	# Update User 
	case'update':
			$QueryObj=new user();
			$user=$QueryObj->getUser($id); 	
			# Update User
			if(isset($_POST['submit'])):
				$QueryObj=new user;
				$QueryObj->saveUser($_POST); 
				$admin_user->set_pass_msg(MSG_EDIT_USER_SUCCESS);
				Redirect(make_admin_url('user', 'list', 'list'));				
			endif;				
		break;
	
	# Get User Trash List	
	case'thrash':
			$QueryObj=new user();
			$record=$QueryObj->listTrashUser(); 			
	break;	
	
	# Delete User 
	case'delete':
		$QueryObj=new user();
		$QueryObj->deleteUser($id);
		$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
		Redirect(make_admin_url('user', 'list', 'list'));  
		break;
		
	# Restore User 
	case'restore':
		$QueryObj=new user();
		$QueryObj->restoreObject($id);
		$admin_user->set_pass_msg(MSG_RESTORE_USER_SUCCESS);
		Redirect(make_admin_url('user', 'thrash', 'thrash'));  
		break;		
	
	# Delete Premanent User 	
	case'del_per':
		$QueryObj=new user();
		$QueryObj->purgeObject($id);
		$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
		Redirect(make_admin_url('user', 'thrash', 'thrash'));  
		break;		
	default:break;
endswitch;
?>
