-- phpMyAdmin SQL Dump
-- version 4.3.8deb0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 09, 2015 at 12:23 PM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `nutrition`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_log`
--

CREATE TABLE IF NOT EXISTS `access_log` (
  `id` int(11) NOT NULL,
  `user` varchar(20) DEFAULT NULL,
  `action` text,
  `ip` varchar(20) DEFAULT NULL,
  `dt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='Admin Access Log';

--
-- Dumping data for table `access_log`
--

INSERT INTO `access_log` (`id`, `user`, `action`, `ip`, `dt`) VALUES
(1, 'admin', 'Action insert performed on 0 in food module', 'fe80::759a:f399:6d2:', '2015-07-01 19:25:39'),
(2, 'admin', 'Action list performed on 0 in plan module', 'fe80::759a:f399:6d2:', '2015-07-06 20:21:35'),
(3, 'admin', 'Action list performed on 0 in plan module', 'fe80::759a:f399:6d2:', '2015-07-06 20:21:55'),
(4, 'admin', 'Action list performed on 0 in plan module', 'fe80::759a:f399:6d2:', '2015-07-06 20:22:04');

-- --------------------------------------------------------

--
-- Table structure for table `activity_factor`
--

CREATE TABLE IF NOT EXISTS `activity_factor` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `add_date` varchar(50) DEFAULT NULL,
  `upd_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_factor`
--

INSERT INTO `activity_factor` (`id`, `name`, `description`, `is_active`, `is_deleted`, `add_date`, `upd_date`) VALUES
(11, 'ABC1', 'dfd', 1, 0, '2015-07-06 13:44:34', '2015-07-06 02:48:00'),
(13, 'XYZ', '', 1, 0, '2015-07-08 08:25:28', '2015-07-07 21:25:28'),
(14, 'CY', '', 1, 0, '2015-07-08 08:25:41', '2015-07-07 21:25:41');

-- --------------------------------------------------------

--
-- Table structure for table `admin_user`
--

CREATE TABLE IF NOT EXISTS `admin_user` (
  `id` int(11) NOT NULL,
  `username` varchar(10) NOT NULL DEFAULT '',
  `password` varchar(10) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL,
  `last_access` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `allow_pages` varchar(255) NOT NULL DEFAULT '*',
  `type` varchar(20) NOT NULL DEFAULT 'Administrator',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_loggedin` tinyint(4) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COMMENT='website control panel scrap table';

--
-- Dumping data for table `admin_user`
--

INSERT INTO `admin_user` (`id`, `username`, `password`, `email`, `last_access`, `allow_pages`, `type`, `is_active`, `is_loggedin`, `is_deleted`) VALUES
(9, 'admin', 'admin', 'rocky.developer002@gmail.com', '2015-07-08 17:16:20', '*', 'Administrator', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `backup`
--

CREATE TABLE IF NOT EXISTS `backup` (
  `id` int(11) NOT NULL,
  `last_updated_dt` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `document` varchar(255) DEFAULT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_deleted` tinyint(1) DEFAULT '0',
  `position` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

CREATE TABLE IF NOT EXISTS `food` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `food_type` varchar(50) DEFAULT NULL,
  `unit` varchar(50) NOT NULL,
  `calories` int(11) DEFAULT NULL,
  `protein` int(11) NOT NULL,
  `fat` int(11) NOT NULL,
  `carb` int(11) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `add_date` varchar(50) DEFAULT NULL,
  `upd_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`id`, `name`, `food_type`, `unit`, `calories`, `protein`, `fat`, `carb`, `description`, `is_active`, `is_deleted`, `add_date`, `upd_date`) VALUES
(1, 'almonds', 'veg', 'grams', 575, 21, 0, 22, 'This is the first', 0, 0, '2015-07-02 13:12:42', '2015-07-06 02:00:46'),
(2, 'apples', 'veg', 'ml', 25, 25, 10, 25, 'dfdfafdfd', 1, 0, '2015-07-02 13:22:20', '2015-07-06 02:01:06'),
(3, 'NN', NULL, 'grams', 25, 25, 42, 35, 'dfdfdf', 1, 1, '2015-07-02 13:27:16', '2015-07-02 03:35:39'),
(7, 'Xyz', 'veg', 'grams', 500, 300, 25, 655, '', 1, 0, '2015-07-06 11:43:56', '2015-07-06 02:00:54'),
(5, 'dfsd', NULL, 'grams', 25, 30, 0, 25, 'dfdf', 1, 1, '2015-07-02 13:29:04', '2015-07-02 03:35:42'),
(11, 'bananas', 'veg', 'grams', 256, 222, 22, 55, '', 1, 0, '2015-07-06 12:45:48', '2015-07-06 01:45:48');

-- --------------------------------------------------------

--
-- Table structure for table `food_cat`
--

CREATE TABLE IF NOT EXISTS `food_cat` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `add_date` varchar(50) DEFAULT NULL,
  `upd_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `food_cat`
--

INSERT INTO `food_cat` (`id`, `name`, `description`, `is_active`, `is_deleted`, `add_date`, `upd_date`) VALUES
(7, 'Protein', 'protein', 1, 0, '2015-07-06 10:30:41', '2015-07-05 23:41:53'),
(8, 'Light Fat', '', 1, 0, '2015-07-06 10:32:53', '2015-07-05 23:32:53'),
(10, 'Fat', '', 1, 0, '2015-07-06 11:40:55', '2015-07-06 00:43:31');

-- --------------------------------------------------------

--
-- Table structure for table `food_cat_rel`
--

CREATE TABLE IF NOT EXISTS `food_cat_rel` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `food_id` int(11) NOT NULL,
  `add_date` varchar(50) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `food_cat_rel`
--

INSERT INTO `food_cat_rel` (`id`, `cat_id`, `food_id`, `add_date`) VALUES
(18, 8, 1, '2015-07-06 13:00:46'),
(17, 7, 11, '2015-07-06 13:00:40'),
(16, 10, 11, '2015-07-06 13:00:40'),
(19, 7, 2, '2015-07-06 13:01:06');

-- --------------------------------------------------------

--
-- Table structure for table `keywords`
--

CREATE TABLE IF NOT EXISTS `keywords` (
  `id` int(11) NOT NULL,
  `page_name` varchar(255) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `method`
--

CREATE TABLE IF NOT EXISTS `method` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `add_date` varchar(50) DEFAULT NULL,
  `upd_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `method`
--

INSERT INTO `method` (`id`, `name`, `description`, `is_active`, `is_deleted`, `add_date`, `upd_date`) VALUES
(15, 'A', 'a', 1, 0, '2015-07-08 09:15:22', '2015-07-07 22:17:42'),
(16, 'C', '', 1, 0, '2015-07-08 09:15:31', '2015-07-07 22:15:31');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL,
  `key` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `value` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `name` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(20) DEFAULT 'select'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `key`, `value`, `name`, `title`, `type`) VALUES
(1, 'ADMIN_EMAIL', 'rocky.developer002@gmail.com', 'email', 'admin email adress', 'text'),
(3, 'SUPPORT_EMAIL', 'rocky.developer002@gmail.com', 'email', 'support email address', 'text'),
(4, 'BCC_EMAIL', 'rocky.developer002@gmail.com', 'email', 'BCC email address', 'text'),
(5, 'SMTP_STATUS', '1', 'SMTP', 'Turn SMTP On', 'select'),
(6, 'SITE_NAME', 'Nutrition App', 'general', 'Application Name', 'text'),
(11, 'PAGE_SIZE', '2', 'pagesize', 'page size for all pages', 'text'),
(28, 'THUMB_HEIGHT', '80', 'Image', 'Thumbnail height', 'text'),
(29, 'THUMB_WIDTH', '105', 'Image', 'Thumbnail width', 'text'),
(32, 'PHONE_NUMBER', '713-332-4675 ', 'general', 'Phone Number', 'text'),
(34, 'ADDRESS1', 'Abc XYZ', 'general', 'Address Line 1', 'text'),
(35, 'ADDRESS2', 'Sector 40D', 'general', 'Address Line 2', 'text'),
(36, 'CITY', 'Xyz', 'general', 'City', 'text'),
(37, 'ZIP_CODE', 'AbX201', 'general', 'Zip Code', 'text'),
(39, 'SMTP_HOST', 'smtp.gmail.com', 'SMTP', 'SMTP Host', 'text'),
(40, 'SMTP_USERNAME', 'rocky.developer002@gmail.com', 'SMTP', 'SMTP Username', 'text'),
(41, 'SMTP_PASSWORD', 'deve#002', 'SMTP', 'SMTP Password', 'text'),
(42, 'MEDIUM_HEIGHT', '225', 'Image', 'Medium Size Image height', 'text'),
(43, 'MEDIUM_WIDTH', '300', 'Image', 'Medium Size Image width', 'text'),
(44, 'FACEBOOK_PAGE', 'http://www.facebook.com/', 'social', 'Facebook Page ', 'text'),
(45, 'TWITTER_PAGE', 'https://twitter.com/twitter', 'social', 'Twitter  Page ', 'text'),
(46, 'YOUTUBE_PAGE', 'http://www.youtube.com/', 'social', 'Youtube Page', 'text'),
(61, 'MAINTENANCE', '1', 'maintenance', 'Maintenance Active', 'select'),
(62, 'MAINTENANCE_TEXT', '', 'maintenance', 'Maintenance Text', 'text'),
(63, 'MAINTENANCE_TITLE', '', 'maintenance', 'Maintenance Title', 'text'),
(80, 'NOT_FOUND', 'Sorry but the page you are looking for has not been found.', 'general', '404 Page Content', 'textarea'),
(81, 'SITE_CACHE_STATUS', '0', 'cache', 'Enable Website Cache', 'select'),
(82, 'SITE_DEBUG_STATUS', '0', 'debugging', 'Enable Debugging', 'select'),
(83, 'FAX_NO', '(509) 472-6808', 'general', 'Fax Number', 'text'),
(84, 'MOBILE_NO', '9898989898', 'general', 'Mobile Number', 'text'),
(109, 'CALORIES_FAT', '25', 'protien', 'Calories in 1 gm of fat', 'text'),
(110, 'CALORIES_CARB', '30', 'protien', 'Calories in 1 gm of carbs', 'text'),
(111, 'CALORIES_PROT', '40', 'protien', 'Calories in 1 gm of protein', 'text');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL DEFAULT '',
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `address` text,
  `contact` varchar(15) DEFAULT NULL,
  `date_of_birth` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` enum('1','0') NOT NULL DEFAULT '0',
  `add_date` varchar(50) DEFAULT NULL,
  `upd_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COMMENT='user login information';

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `email`, `gender`, `address`, `contact`, `date_of_birth`, `is_active`, `is_deleted`, `add_date`, `upd_date`) VALUES
(85, 'AAA', 'kumar', 'test@gmail.com', 'male', 'dfd', '564546654', '1988-07-22', 1, '0', '2015-07-03 09:32:27', '2015-07-06 23:44:16'),
(86, 'Robert', 'brown', 'rocky.developer007@gmail.com', 'male', 'Abc xyz Addres', '2587455', '1985-07-30', 1, '0', '2015-07-03 09:34:46', '2015-07-06 23:44:24'),
(88, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, '1', '2015-07-07 12:49:58', '2015-07-07 01:50:02');

-- --------------------------------------------------------

--
-- Table structure for table `web_stat`
--

CREATE TABLE IF NOT EXISTS `web_stat` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(40) DEFAULT NULL,
  `on_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_log`
--
ALTER TABLE `access_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_factor`
--
ALTER TABLE `activity_factor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_user`
--
ALTER TABLE `admin_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backup`
--
ALTER TABLE `backup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `food`
--
ALTER TABLE `food`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `food_cat`
--
ALTER TABLE `food_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `food_cat_rel`
--
ALTER TABLE `food_cat_rel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keywords`
--
ALTER TABLE `keywords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `method`
--
ALTER TABLE `method`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_stat`
--
ALTER TABLE `web_stat`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_log`
--
ALTER TABLE `access_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `activity_factor`
--
ALTER TABLE `activity_factor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `admin_user`
--
ALTER TABLE `admin_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `backup`
--
ALTER TABLE `backup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `food`
--
ALTER TABLE `food`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `food_cat`
--
ALTER TABLE `food_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `food_cat_rel`
--
ALTER TABLE `food_cat_rel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `keywords`
--
ALTER TABLE `keywords`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `method`
--
ALTER TABLE `method`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `web_stat`
--
ALTER TABLE `web_stat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;