	<!-- BEGIN PAGE SIDEBAR -->
	<div class="page-sidebar">
		<nav class="navbar" role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="clearfix">
				<!-- Toggle Button -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-siderbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="toggle-icon">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</span> 
				</button>
				<!-- End Toggle Button -->
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="nav-collapse collapse navbar-collapse page-siderbar-collapse">
				
				<ul class="nav navbar-nav">
					<li class="<?=($section=='list' && $type=='general')?'active':''?>">
						<a href="<?=make_admin_url('setting','list','list&type=general')?>">
							<i class="icon-home "></i>
							General Settings 
						</a>
					</li>
					<li class="<?=($section=='list' && $type=='email')?'active':''?>">
						<a href="<?=make_admin_url('setting','list','list&type=email')?>">
							<i class="fa fa-envelope"></i>
							Email Settings 
						</a>
					</li>	
					<li class="<?=($section=='list' && $type=='social')?'active':''?>">
						<a href="<?=make_admin_url('setting','list','list&type=social')?>">
							<i class="fa fa-share-alt"></i>
							Social Media 
						</a>
					</li>
									
									
				</ul>
			</div>
			
		</nav>
	</div>
	<!-- END PAGE SIDEBAR -->