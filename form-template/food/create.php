<!-- Page Content Start -->
<div class="page-content">
    <!-- BEGIN BREADCRUMBS -->
    <div class="breadcrumbs">
        <h1>Manage Foods</h1>
        <ol class="breadcrumb">
            <li><a href="<?= make_admin_url('home') ?>">Home</a></li>
            <li><a href="<?= make_admin_url('food') ?>">View List</a></li>
            <li class="active">Add New</li>
        </ol>
    </div>
    <!-- END BREADCRUMBS -->

    <?php
    /* display message */
    display_message(1);
    ?>	

    <!-- Left Bar Sortcut-->
    <?php include_once(DIR_FS_SITE . '/form-template/' . $modName . '/shortcut.php'); ?>  


    <!-- BEGIN PAGE CONTAINER -->
    <div class="page-container">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box sky-blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="fa fa-plus"></i>Add New Food</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form form-horizontal validation" action="<?php echo make_admin_url('food', 'insert', 'insert') ?>" method="POST" enctype="multipart/form-data" >
                            <div class="form-body">	
                                <div class="row">	
                                    <div class="col-md-7">
                                        <h3 class='form_heading width80'>Food Information</h3>
                                        <div class="form-group">
                                            <label for="name" class="col-md-3 control-label padding-right-none">Food Name</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control validate[required]" id="name" name="name" placeholder="Food Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="food_type" class="col-md-3 control-label padding-right-none">Food Type</label>
                                            <div class="col-md-7">
                                                <select class="form-control" name="food_type" >
                                                    <option value='veg'>Vegetarian</option>
                                                    <option value='non-veg'>Non Vegetarian</option>
                                                </select>
                                            </div>
                                        </div>											
                                        <div class="form-group">
                                            <label for="unit" class="col-md-3 control-label padding-right-none">Unit (Grams/Ml)</label>
                                            <div class="col-md-7">
                                                <select class="form-control" name="unit" >
                                                    <option value='grams'>Grams</option>
                                                    <option value='ml'>ML</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="calories" class="col-md-3 control-label padding-right-none">Calories</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control validate[required,custom[number]]" id="calories" name="calories" placeholder="Calories">
                                            </div>
                                        </div>		
                                        <div class="form-group">
                                            <label for="protein" class="col-md-3 control-label padding-right-none">Protein (per gram/ml)</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control validate[required,custom[number]]" id="protein" name="protein" placeholder="Protein">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="fat" class="col-md-3 control-label padding-right-none">Fat (per gram/ml)</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control validate[required,custom[number]]" id="fat" name="fat" placeholder="Fat">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="carb" class="col-md-3 control-label padding-right-none">Carb (per gram/ml)</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control validate[required,custom[number]]" id="carb" name="carb" placeholder="Carb">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="description" class="col-md-3 control-label padding-right-none">Description</label>
                                            <div class="col-md-7">
                                                <textarea class="form-control" rows="3" name='description' placeholder="Description"></textarea>
                                            </div>
                                        </div>										
                                        <div class="form-group">
                                            <label for="name" class="col-md-3 control-label padding-right-none">Active</label>
                                            <div class="col-md-7 padding-top-checkbox">
                                                <input type="checkbox" name='is_active' checked value='1'>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <h3 class='form_heading width80'>Category Information</h3>

                                        <? if (!empty($foodCatList)): ?>
                                            <? foreach ($foodCatList as $key => $val): ?>
                                                <div class="form-group checkbox_div">
                                                    <div class="checkbox-list">
                                                        <label class="checkbox-inline">

                                                            <input type="checkbox" name='cat[]' id="inlineCheckbox21" value="<?= $val->id ?>">
                                                            <?= $val->name ?> 
                                                        </label>
                                                    </div>
                                                </div>
                                            <? endforeach;
                                            ?>
                                        <? else: ?>
                                            Sorry No Category Found..!
                                        <? endif; ?>											
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <div class="col-md-offset-2 col-md-10">
                                        <a href="<?php echo make_admin_url('food', 'list', 'list'); ?>" class="btn btn-default" name="cancel" > Cancel</a>											
                                        <button type="submit" name='submit' class="btn blue">Submit</button>
                                    </div>
                                </div>
                                <div class='clear'></div>
                        </form>										
                    </div>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>		

    </div>

</div>
<!-- END PAGE CONTAINER -->
</div>
<!-- PAGE CONTENT END -->