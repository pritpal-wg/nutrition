<!-- Page Content Start -->
<div class="page-content">
	<!-- BEGIN BREADCRUMBS -->
	<div class="breadcrumbs">
		<h1>Manage Foods</h1>
		<ol class="breadcrumb">
			<li><a href="<?=make_admin_url('home')?>">Home</a></li>
			<li><a href="<?=make_admin_url('food')?>">View Food</a></li>
			<li class="active">Trash Records</li>
		</ol>
	</div>
	<!-- END BREADCRUMBS -->
	
	<?php 
	/* display message */
	display_message(1);
	?>	

	<!-- Left Bar Sortcut-->
	<?php  include_once(DIR_FS_SITE.'/form-template/'.$modName.'/shortcut.php');?>  
				

	<!-- BEGIN PAGE CONTAINER -->
	<div class="page-container">
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PORTLET-->
					<div class="portlet box sky-blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-list"></i>Trash Records</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body">
								<table class="table table-striped table-bordered table-hover dataTable no-footer" id="sample_1" role="grid">
									<thead>
										 <tr>
												<th style="width:65px;">Sr. No.</th>
												<th class="hidden-480 sorting_disabled">Name</th>
												<th class="hidden-480 sorting_disabled">Calories</th>
												<th class="hidden-480 sorting_disabled">Protein</th>
												<th class="hidden-480 sorting_disabled">Fat</th>
												<th class="hidden-480 sorting_disabled">Carb</th>
												<th class="hidden-480 sorting_disabled">Serving Size</th>
												<th style='text-align:right;'>Action</th>
										</tr>
									</thead>
                                        <? if(!empty($record)):?>
										<tbody>
                                            <?php $sr=1;foreach($record as $key=>$object):?>
											<tr>
											<td><?=$sr?>.</td>
											<td><?=ucfirst($object->name)?></td>
											<td><?=number_format($object->calories,1)?></td>
											<td><?=number_format($object->protein,1)?></td>
											<td><?=number_format($object->fat,1)?></td>
											<td><?=number_format($object->carb,1)?></td>
											<td><?=ucfirst($object->unit)?></td>
											<td class='right'>
												<a class="btn mini blue icn-only tooltips margin-left-none margin-right-none" href="<?php echo make_admin_url('food', 'restore', 'restore', 'id='.$object->id)?>" title="click here to restore this record" onclick="return confirm('Are you sure? You are restore this record.');"><i class="fa fa-refresh"></i></a>&nbsp;&nbsp;
												<a class="btn mini red icn-only tooltips margin-left-none margin-right-none" href="<?php echo make_admin_url('food', 'del_per', 'del_per', 'id='.$object->id.'&delete=1')?>" onclick="return confirm('Are you sure? You are deleting this record permanently.');" title="click here to delete this record"><i class="fa fa-trash"></i></a>
											</td>
											</tr>
                                            <?php $sr++;
												endforeach;?>
										</tbody>
									   <?php endif;?>  
								</table>
							</div>
					</div>
			</div>
				<!-- END PORTLET-->
		</div>		
		
		</div>
	
	</div>
	<!-- END PAGE CONTAINER -->
</div>
<!-- PAGE CONTENT END -->