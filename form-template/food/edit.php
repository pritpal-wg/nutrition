<!-- Page Content Start -->
<div class="page-content">
    <!-- BEGIN BREADCRUMBS -->
    <div class="breadcrumbs">
        <h1>Manage Foods</h1>
        <ol class="breadcrumb">
            <li><a href="<?= make_admin_url('home') ?>">Home</a></li>
            <li><a href="<?= make_admin_url('food') ?>">View List</a></li>
            <li class="active">Edit</li>
        </ol>
    </div>
    <!-- END BREADCRUMBS -->
    <?php
    /* display message */
    display_message(1);
    ?>
    <!-- Left Bar Sortcut-->
    <?php include_once(DIR_FS_SITE . '/form-template/' . $modName . '/shortcut.php'); ?>  
    <!-- BEGIN PAGE CONTAINER -->
    <div class="page-container">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box sky-blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="fa fa-plus"></i>Edit Category</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form form-horizontal validation" action="<?php echo make_admin_url('food', 'update', 'update') ?>" method="POST" enctype="multipart/form-data" >
                            <div class="form-body">	
                                <div class="row">	
                                    <div class="col-md-7">	
                                        <h3 class='form_heading width80'>Food Information</h3>	
                                        <div class="form-group">
                                            <label for="name" class="col-md-3 control-label padding-right-none">Food Name</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control validate[required]" id="name" name="name" value='<?= $food->name ?>' placeholder="Food Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="food_type" class="col-md-3 control-label padding-right-none">Food Type</label>
                                            <div class="col-md-7">
                                                <select class="form-control" name="food_type" >
                                                    <option value='veg' <?= ($food->food_type == 'veg') ? 'selected' : '' ?>>Vegetarian</option>
                                                    <option value='non-veg' <?= ($food->food_type == 'non-veg') ? 'selected' : '' ?>>Non Vegetarian</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="unit" class="col-md-3 control-label padding-right-none">Unit (Grams/Ml)</label>
                                            <div class="col-md-7">
                                                <select class="form-control" name="unit" >
                                                    <option value='grams' <?= ($food->unit == 'grams') ? 'selected' : '' ?>>Grams</option>
                                                    <option value='ml' <?= ($food->unit == 'ml') ? 'selected' : '' ?>>ML</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="calories" class="col-md-3 control-label padding-right-none">Calories</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control validate[required,custom[number]]" id="calories" value='<?= $food->calories ?>' name="calories" placeholder="Calories">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="protein" class="col-md-3 control-label padding-right-none">Protein (per gram/ml)</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control validate[required,custom[number]]" id="protein" value='<?= $food->protein ?>' name="protein" placeholder="Protein">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="fat" class="col-md-3 control-label padding-right-none">Fat (per gram/ml)</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control validate[required,custom[number]]" id="fat" value='<?= $food->fat ?>' name="fat" placeholder="Fat">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="carb" class="col-md-3 control-label padding-right-none">Carb (per gram/ml)</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control validate[required,custom[number]]" id="carb" value='<?= $food->carb ?>' name="carb" placeholder="Carb">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="description" class="col-md-3 control-label padding-right-none">Description</label>
                                            <div class="col-md-7">
                                                <textarea class="form-control" rows="3" name='description' placeholder="Description"><?= $food->description ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="name" class="col-md-3 control-label padding-right-none">Active</label>
                                            <div class="col-md-7 padding-top-checkbox">
                                                <input type="checkbox" name='is_active' <?= ($food->is_active == '1') ? 'checked' : '' ?> value='1'>
                                                <input type='hidden' name='id' value="<?= $food->id ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <h3 class='form_heading width80'>Category Information</h3>
                                        <? if (!empty($foodCatList)): ?>
                                            <select multiple name="cat[]" class="form-control" data-toggle="chosen">
                                                <?php echo cats_loop($foodCatList, '', $catRel) ?>
                                                <? // foreach ($foodCatList as $key => $val): ?>
                                                    <!--<option value="<?= $val->id ?>" <?= (in_array($val->id, $catRel)) ? 'selected' : '' ?>><?= $val->name ?></option>-->
                                                <? // endforeach ?>
                                            </select>
                                        <? else: ?>
                                            Sorry No Category Found..!
                                        <? endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="col-md-offset-2 col-md-10">
                                    <a href="<?php echo make_admin_url('food_cat', 'list', 'list'); ?>" class="btn btn-default" name="cancel" > Cancel</a>
                                    <button type="submit" name='submit' class="btn blue">Submit</button>
                                </div>
                            </div>
                            <div class='clear'></div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
</div>
<!-- END PAGE CONTAINER -->
</div>
<!-- PAGE CONTENT END -->