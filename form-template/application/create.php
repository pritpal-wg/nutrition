<!-- Page Content Start -->
<div class="page-content">
	<!-- BEGIN BREADCRUMBS -->
	<div class="breadcrumbs">
		<h1>Manage Foods</h1>
		<ol class="breadcrumb">
			<li><a href="<?=make_admin_url('home')?>">Home</a></li>
			<li><a href="<?=make_admin_url('application')?>">Activity Factor</a></li>
			<li class="active">Add New</li>
		</ol>
	</div>
	<!-- END BREADCRUMBS -->
	
	<?php 
	/* display message */
	display_message(1);
	?>	

	<!-- Left Bar Sortcut-->
	<?php  include_once(DIR_FS_SITE.'/form-template/'.$modName.'/shortcut.php');?>  
				

	<!-- BEGIN PAGE CONTAINER -->
	<div class="page-container">
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PORTLET-->
					<div class="portlet box sky-blue">
							<div class="portlet-title">
								<div class="caption"><i class="fa fa-plus"></i>Add New</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body">
											
								<form class="form form-horizontal validation" action="<?php echo make_admin_url('application', 'insert', 'insert')?>" method="POST" enctype="multipart/form-data" >
									<div class="form-body">	
										<div class="form-group">
											<label for="name" class="col-md-2 control-label padding-right-none">Name</label>
											<div class="col-md-4">
												<input type="text" class="form-control validate[required]" id="name" name="name" placeholder="Factor Name">
											</div>
										</div>
										<div class="form-group">
											<label for="description" class="col-md-2 control-label padding-right-none">Description</label>
											<div class="col-md-4">
												<textarea class="form-control" rows="3" name='description' placeholder="Description"></textarea>
											</div>
										</div>										
										<div class="form-group">
											<label for="name" class="col-md-2 control-label padding-right-none">Active</label>
											<div class="col-md-4 padding-top-checkbox">
												<input type="checkbox" name='is_active' checked value='1'>
											</div>
										</div>
									</div>
								
									<div class="form-actions">
										<div class="col-md-offset-2 col-md-10">
											<a href="<?php echo make_admin_url('application', 'list', 'list');?>" class="btn btn-default" name="cancel" > Cancel</a>											
											<button type="submit" name='submit' class="btn blue">Submit</button>
										</div>
									</div>
									<div class='clear'></div>
								</form>										
							</div>
					</div>
			</div>
				<!-- END PORTLET-->
		</div>		
		
		</div>
	
	</div>
	<!-- END PAGE CONTAINER -->
</div>
<!-- PAGE CONTENT END -->