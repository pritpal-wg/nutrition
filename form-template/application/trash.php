<!-- Page Content Start -->
<div class="page-content">
	<!-- BEGIN BREADCRUMBS -->
	<div class="breadcrumbs">
		<h1>Trashed Activity Factors</h1>
		<ol class="breadcrumb">
			<li><a href="<?=make_admin_url('home')?>">Home</a></li>
			<li class="active">Trashed Activity Factors</li>
		</ol>
	</div>
	<!-- END BREADCRUMBS -->
	
	<?php 
	/* display message */
	display_message(1);
	?>	

	<!-- Left Bar Sortcut-->
	<?php  include_once(DIR_FS_SITE.'/form-template/'.$modName.'/shortcut.php');?>  
				

	<!-- BEGIN PAGE CONTAINER -->
	<div class="page-container">
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PORTLET-->
					<div class='pull-right'>
                                                <a href="<?=make_admin_url('application','list','list')?>" class='btn sky-blue-btn tooltips ' title='Add New Activity Factor'><i class="fa fa-chevron-left"></i> Back</a>
						<a href="<?=make_admin_url('application','insert','insert')?>" class='btn sky-blue-btn tooltips ' title='Add New Activity Factor'><i class="icon-plus "></i> Add New </a>
					</div>
					<div class="clear"><br/></div>
					<div class="portlet box sky-blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-list"></i>Trashed Activity Factors</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body">
								<table class="table table-striped table-bordered table-hover dataTable no-footer" id="sample_1" role="grid">
									<thead>
										 <tr>
												<th style="width:65px;">Sr. No.</th>
												<th class="hidden-480 sorting_disabled">Name</th>
												<th style='text-align:right;'>Action</th>
										</tr>
									</thead>
                                        <? if(!empty($record)):?>
										<tbody>
                                            <?php $sr=1;foreach($record as $key=>$object):?>
											<tr>
											<td><?=$sr?>.</td>
											<td><?=ucfirst($object->name)?></td>
											<td class='right'>
												<a class="btn mini green icn-only tooltips margin-left-none margin-right-none" href="<?php echo make_admin_url('application', 'restore', 'restore', 'id='.$object->id)?>" title="click here to restore this record"><i class="fa fa-undo"></i></a>
												<a class="btn mini red icn-only tooltips margin-left-none margin-right-none" href="<?php echo make_admin_url('application', 'delete', 'delete', 'id='.$object->id.'&delete=1&force=1')?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record"><i class="fa fa-trash-o"></i></a>
											</td>
											</tr>
                                            <?php $sr++;
												endforeach;?>
										</tbody>
									   <?php endif;?>  
								</table>
							</div>
					</div>
			</div>
				<!-- END PORTLET-->
		</div>		
		
		</div>
	
	</div>
	<!-- END PAGE CONTAINER -->
</div>
<!-- PAGE CONTENT END -->