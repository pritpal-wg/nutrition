	<!-- BEGIN PAGE SIDEBAR -->
	<div class="page-sidebar">
		<nav class="navbar" role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="clearfix">
				<!-- Toggle Button -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-siderbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="toggle-icon">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</span> 
				</button>
				<!-- End Toggle Button -->
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="nav-collapse collapse navbar-collapse page-siderbar-collapse">
				
				<ul class="nav navbar-nav">
					<li class="<?=($Page=='application'&&$section=='list')?'active':''?>">
						<a href="<?=make_admin_url('application','list','list')?>">
							<i class="icon-home "></i>
							Activity Factor  
						</a>
					</li>
					<li class="<?=($Page=='method')?'active':''?>">
						<a href="<?=make_admin_url('method','list','list')?>">
							<i class="icon-list "></i>
							Method Used  
						</a>
					</li>									
					<li class="<?=($section=='list1')?'active':''?>">
						<a href="<?=make_admin_url('application','list1','list1')?>">
							<i class="fa fa-envelope"></i>
							Calories Settings 
						</a>
					</li>	
									
				</ul>
			</div>
			
		</nav>
	</div>
	<!-- END PAGE SIDEBAR -->