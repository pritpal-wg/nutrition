<!-- Page Content Start -->
<div class="page-content">
    <!-- BEGIN BREADCRUMBS -->
    <div class="breadcrumbs">
        <h1>Nutrition Plans</h1>
        <ol class="breadcrumb">
            <li><a href="<?= make_admin_url('home') ?>">Home</a></li>
            <li><a href="<?= make_admin_url('plan', 'list', 'list') ?>">Nutrition Plans</a></li>
            <li class="active">Training Day</li>
        </ol>
    </div>
    <!-- END BREADCRUMBS -->
    <?php display_message(1) ?>
    <!-- Left Bar Sortcut-->
    <?php include_once(DIR_FS_SITE . '/form-template/' . $modName . '/shortcut.php'); ?>
    <!-- BEGIN PAGE CONTAINER -->
    <div class="page-container">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box sky-blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="fa fa-list" style="color: #fff"></i>Training Day Meal Plan - <small><?php echo $user->first_name . ' ' . $user->last_name ?></small></div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="margin-bottom: 0">
                            <div class="panel panel-info">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="text-decoration: none !important" title="Click to Show/Hide">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title" style="color: #000">Information</h4>
                                    </div>
                                </a>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body well well-sm" style="margin-bottom: 0">
                                        <div class="row-fluid">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-4 col-md-offset-1">
                                                        <div class="form-horizontal">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-6" style="margin-top: 0;padding-top: 0">Name :</label>
                                                                <div class="col-md-6"><?php echo $user->first_name . ' ' . $user->last_name ?></div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-6" style="margin-top: 0;padding-top: 0">Gender :</label>
                                                                <div class="col-md-6"><?php echo ucfirst($user->gender) ?></div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-6" style="margin-top: 0;padding-top: 0">Contact :</label>
                                                                <div class="col-md-6"><?php echo $user->contact ?></div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-6" style="margin-top: 0;padding-top: 0">Age :</label>
                                                                <div class="col-md-6"><?php echo $t_plan->age ?> Years</div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-6" style="margin-top: 0;padding-top: 0">Proteins:</label>
                                                                <div class="col-md-6">
                                                                    <input type="hidden" id="target_proteins" value="<?php echo $t_plan->protein_per_day ?>" />
                                                                    <?php echo $t_plan->protein_per_day ?>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-6" style="margin-top: 0;padding-top: 0">Meals :</label>
                                                                <div class="col-md-6"><?php echo $t_plan->total_meals ?></div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-6" style="margin-top: 0;padding-top: 0">Training Time:</label>
                                                                <div class="col-md-6"><?php echo $t_plan->training_time ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-md-offset-1">
                                                        <div class="form-horizontal">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-6" style="margin-top: 0;padding-top: 0">Weight :</label>
                                                                <div class="col-md-6"><?php echo $t_plan->weight ?> kg</div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-6" style="margin-top: 0;padding-top: 0">Height :</label>
                                                                <div class="col-md-6"><?php echo $t_plan->height ?> cm</div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-6" style="margin-top: 0;padding-top: 0">Body Fat :</label>
                                                                <div class="col-md-6"><?php echo $t_plan->fats ?> %</div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-6" style="margin-top: 0;padding-top: 0">BMR :</label>
                                                                <div class="col-md-6"><?php echo $t_plan->bmr ?></div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-6" style="margin-top: 0;padding-top: 0">Target Calories:</label>
                                                                <div class="col-md-6">
                                                                    <input type="hidden" id="target_calories" value="<?php echo $t_plan->target_calories ?>" />
                                                                    <input type="hidden" id="day_carbs_fats" value="<?php echo $t_plan->day_carbs_fats ?>" />
                                                                    <?php echo $t_plan->target_calories ?>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-6" style="margin-top: 0;padding-top: 0">Wake Up Time:</label>
                                                                <div class="col-md-6">
                                                                    <?php echo $t_plan->wake_up_time ?>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-6" style="margin-top: 0;padding-top: 0">Sleep Time:</label>
                                                                <div class="col-md-6">
                                                                    <?php echo $t_plan->sleep_time ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="<?php echo make_admin_url('plan', 'update', 'update', 'id=' . $id) ?>" class="btn btn-sm btn-default btn-block"><i class="fa fa-pencil"></i> Edit the Information</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr style="margin: 10px 0" />
                        <div class="row">
                            <?php $original_t_plan_meals = $t_plan_meals ?>
                            <?php
//                            pr($t_plan);
                            $plan_info_id = $t_plan->id;
                            $tot_meals = $t_plan->total_meals;
                            $wake_up_time = date("H:i", strtotime($t_plan->wake_up_time));
                            $sleep_time = date("H:i", strtotime($t_plan->sleep_time));
                            list($hours, $minutes) = explode(':', $wake_up_time);
                            $startTimestamp = mktime($hours, $minutes);
                            list($hours, $minutes) = explode(':', $sleep_time);
                            $endTimestamp = mktime($hours, $minutes);
                            $seconds = $endTimestamp - $startTimestamp;
                            $minutes = ($seconds / 60) % 60;
                            $hours = floor($seconds / (60 * 60));
                            $meal_hours_diff = ceil($hours / $tot_meals);
                            $this_meal_time = date("h:i A", strtotime($t_plan->wake_up_time . ' - ' . $meal_hours_diff . 'hours + 30min'));
                            ?>
                            <form method="post">
                                <div class="col-md-12">
                                    <input type="hidden" id="plan_info_id" name="plan_info_id" value="<?php echo $plan_info_id ?>" />
                                    <span style="margin-left: 10px">Meals: <b><?php echo $t_plan->total_meals ?></b></span>
                                    <div class="margin-top-10"></div>
                                    <?php for ($i = 1; $i <= $t_plan->total_meals; $i++) { ?>
                                        <?php
                                        $t_plan_meal = array_shift($t_plan_meals);
                                        $saved = is_object($t_plan_meal);
                                        $this_meal_time = $saved ? $t_plan_meal->meal_time : date('h:i A', strtotime($this_meal_time . ' +' . ($meal_hours_diff) . ' hours'));
                                        $meal_name = $saved ? $t_plan_meal->meal_name : 'Meal Name';
                                        $meal_category = $saved ? $t_plan_meal->meal_category : 'BreakFast';
                                        $meal_proteins = $saved ? $t_plan_meal->meal_proteins : $t_plan->meal_proteins;
                                        $meal_carbs = $saved ? $t_plan_meal->meal_carbs : $t_plan->meal_carbs;
                                        $meal_fats = $saved ? $t_plan_meal->meal_fats : $t_plan->meal_fats;
                                        $meal_carbs_fats = $meal_carbs + $meal_fats;
                                        $meal_comments = $saved ? $t_plan_meal->meal_comments : "";
                                        $meal_calories = $saved ? $t_plan_meal->meal_calories : $t_plan->meal_calories;
                                        ?>
                                        <?php if ($saved) { ?><input name="plan_meal_id[<?= $i ?>]" type="hidden" value="<?= $t_plan_meal->id ?>" /><?php } ?>
                                        <div id="meal_plan_div" data-meal="<?= $i ?>">
                                            <div class="well" style="border-bottom: solid 3px lightgray">
                                                <div class="pull-right">
                                                    <div class="btn-group btn-group-sm">
                                                        <button type="button" class="btn btn-warning" data-toggle="collapse" data-target="#collapse<?php echo $i ?>"><i class="fa fa-edit"></i></button>
                                                        <button type="button" class="btn btn-danger"><i class="fa fa-times"></i></button>
                                                    </div>
                                                </div>
                                                <span><i class="fa fa-chevron-circle-right" style="font-size: 17px"></i></span>
                                                <span style="margin-left: 10px"><strong><?php echo $i ?>:</strong></span>
                                                <span style="margin-left: 10px"><i class="meal_time"><?php echo $this_meal_time ?></i></span>
                                                <span style="margin-left: 35px"><i class="meal_name"><?= $meal_name ?></i></span>
                                                <span style="margin-left: 35px"><b class="meal_category"><?= $meal_category ?></b></span>
                                                <span style="margin-left: 25px">(<i>Proteins:</i> <span class="meal_proteins"><?php echo $meal_proteins ?></span>)</span>
                                                <span style="margin-left: 25px">(<i>Calories:</i> <span class="meal_calories"><?php echo $meal_calories ?></span>)</span>
                                                <span style="margin-left: 25px">(<i>Fats/Carbs Ratio:</i> <span class="meal_fats_carbs"><?= round(($meal_fats / $meal_carbs_fats) * 100) ?>/<?= 100 - round(($meal_fats / $meal_carbs_fats) * 100) ?></span>)</span>
                                                <input type="hidden" id="meal_carbs_fats" value="<?php echo $meal_carbs_fats ?>" />
                                                <div class="collapse" data-id="collapse_div" id="collapse<?php echo $i ?>">
                                                    <div class="row" style="padding: 20px 5px">
                                                        <div class="col-md-12">
                                                            <div class="form-inline">
                                                                <div class="form-group">
                                                                    <label>Name:</label><br />
                                                                    <input type="text" class="form-control" value="<?= $meal_name ?>" id="meal_name" name="meal_name[<?= $i ?>]" style="width: 120px" />
                                                                </div>
                                                                <div class="form-group" style="margin-left: 40px">
                                                                    <label>Category:</label><br />
                                                                    <?php
                                                                    $_categories = array(
                                                                        'Breakfast',
                                                                        'Mid Morning',
                                                                        'Lunch',
                                                                        'Mid Afternoon',
                                                                        'Dinner',
                                                                        'Pre Bed',
                                                                        'Pre Meal',
                                                                        'Post Shake',
                                                                        'Post Meal',
                                                                    );
                                                                    ?>
                                                                    <select name="meal_category[<?= $i ?>]" id="meal_category" class="form-control" style="width: 120px">
                                                                        <?php foreach ($_categories as $_category) { ?>
                                                                            <option<?= $meal_category == $_category ? ' selected' : '' ?>><?php echo $_category ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group" style="margin-left: 40px">
                                                                    <label>Proteins:</label><br />
                                                                    <input type="number" class="form-control" id="meal_proteins" name="meal_proteins[<?= $i ?>]" value="<?php echo $meal_proteins ?>" style="width: 100px" />
                                                                </div>
                                                                <div class="form-group" style="margin-left: 40px">
                                                                    <label>Calories:</label><br />
                                                                    <input type="number" class="form-control" id="meal_calories" name="meal_calories[<?= $i ?>]" value="<?php echo $meal_calories ?>" style="width: 100px" />
                                                                </div>
                                                                <div class="form-group" style="margin-left: 30px">
                                                                    <input type="hidden" name="meal_fats[<?= $i ?>]" id="meal_fats" value="<?php echo $meal_fats ?>" />
                                                                    <input type="hidden" name="meal_carbs[<?= $i ?>]" id="meal_carbs" value="<?php echo $meal_carbs ?>" />
                                                                    <label>Fats:</label><br />
                                                                    <input type="number" max="100" min="0" id="meal_fats_carbs" class="form-control" value="<?= round(($meal_fats / $meal_carbs_fats) * 100) ?>" style="width: 60px;padding-right: 5px" />
                                                                </div>
                                                                <div class="form-group" style="margin-left: 20px">
                                                                    <label>Time</label><br />
                                                                    <input type="text" id="meal_time" readonly name="meal_time[<?= $i ?>]" class="form-control timepicker timepicker-no-seconds" style="background-color: #fff;cursor: pointer;width: 100px" value="<?php echo $this_meal_time ?>" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <table class="table table-condensed">
                                                            <thead>
                                                                <tr class="success">
                                                                    <th>
                                                                        <span class="pull-right">
                                                                            <button class="btn btn-success btn-xs add_food"><i class="fa fa-plus"></i></button>
                                                                        </span>
                                                                        Foods
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <th>
                                                                        <?php include __DIR__ . '/t_plan_foods.php'; ?>
                                                                    </th>
                                                                </tr>
                                                            </tbody>
                                                            <tfoot>
                                                                <tr class="success">
                                                                    <th>Comments</th>
                                                                </tr>
                                                                <tr>
                                                                    <th>
                                                                        <textarea name="meal_comments[<?= $i ?>]" class="form-control"><?= $meal_comments ?></textarea>
                                                                    </th>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="col-md-12">
                                    <button name="submit" value="1" class="btn btn-success btn-lg btn-block" type="submit"><i class="fa fa-check-square-o"></i> <?= $saved ? 'Update' : 'Save' ?> Plan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
</div>
<script>
    var foods = [];
<?php foreach ($foods as $food) { ?>
        var food = {
            "id": "<?php echo $food->id ?>",
            "name": "<?php echo $food->name ?>",
            "unit": "<?php echo $food->unit ?>",
            "calories": "<?php echo $food->calories ?>",
            "protein": "<?php echo $food->protein ?>",
            "fat": "<?php echo $food->fat ?>",
            "carb": "<?php echo $food->carb ?>",
        };
        foods.push(food);
<?php } ?>
</script>