<!-- Page Content Start -->
<div class="page-content">
    <!-- BEGIN BREADCRUMBS -->
    <div class="breadcrumbs">
        <h1>Nutrition Plans</h1>
        <ol class="breadcrumb">
            <li><a href="<?= make_admin_url('home') ?>">Home</a></li>
            <li><a href="<?= make_admin_url('plan', 'list', 'list') ?>">Nutrition Plans</a></li>
            <li class="active">Plan Report</li>
        </ol>
    </div>
    <!-- END BREADCRUMBS -->
    <?php display_message(1) ?>
    <!-- Left Bar Sortcut-->
    <?php include_once(DIR_FS_SITE . '/form-template/' . $modName . '/shortcut.php'); ?>
    <!-- BEGIN PAGE CONTAINER -->
    <div class="page-container">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box sky-blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="fa fa-list" style="color: #fff"></i>Meal Plan Report - <small><?php echo $user->first_name . ' ' . $user->last_name ?></small></div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div>
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs nav-justified" role="tablist">
                                        <li role="presentation" class="active"><a class="btn blue" href="#training_day" aria-controls="home" role="tab" data-toggle="tab">Training Day</a></li>
                                        <li role="presentation"><a class="btn blue" href="#non_training_day" aria-controls="profile" role="tab" data-toggle="tab">Non-Training Day</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content" style="overflow-x: hidden;overflow-y: scroll;max-height: 900px">
                                        <div role="tabpanel" class="tab-pane active" id="training_day">
                                            <div class="pull-right no-print">
                                                <button id="t_report_print" class="btn btn-xs btn-primary"><i class="fa fa-print"></i> Print Training Day Plan</button>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-xs-4 col-xs-offset-1">
                                                    <div class="form-horizontal">
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Name :</label>
                                                            <div class="col-xs-6"><?php echo $user->first_name . ' ' . $user->last_name ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Gender :</label>
                                                            <div class="col-xs-6"><?php echo ucfirst($user->gender) ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Contact :</label>
                                                            <div class="col-xs-6"><?php echo $user->contact ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Age :</label>
                                                            <div class="col-xs-6"><?php echo $t_plan->age ?> Years</div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Proteins:</label>
                                                            <div class="col-xs-6">
                                                                <?php echo $t_plan->protein_per_day ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Meals :</label>
                                                            <div class="col-xs-6"><?php echo $t_plan->total_meals ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Training Time:</label>
                                                            <div class="col-xs-6"><?php echo $t_plan->training_time ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-xs-offset-1">
                                                    <div class="form-horizontal">
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Weight :</label>
                                                            <div class="col-xs-6"><?php echo $t_plan->weight ?> kg</div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Height :</label>
                                                            <div class="col-xs-6"><?php echo $t_plan->height ?> cm</div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Body Fat :</label>
                                                            <div class="col-xs-6"><?php echo $t_plan->fats ?> %</div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">BMR :</label>
                                                            <div class="col-xs-6"><?php echo $t_plan->bmr ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Calories:</label>
                                                            <div class="col-xs-6">
                                                                <?php echo $t_plan->day_calories ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Wake Up Time:</label>
                                                            <div class="col-xs-6">
                                                                <?php echo $t_plan->wake_up_time ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Sleep Time:</label>
                                                            <div class="col-xs-6">
                                                                <?php echo $t_plan->sleep_time ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr />
                                            <div class="row">
                                                <?php
                                                $original_t_plan_meals = $t_plan_meals;
                                                $plan_info_id = $t_plan->id;
                                                $tot_meals = $t_plan->total_meals;
                                                $wake_up_time = date("H:i", strtotime($t_plan->wake_up_time));
                                                $sleep_time = date("H:i", strtotime($t_plan->sleep_time));
                                                list($hours, $minutes) = explode(':', $wake_up_time);
                                                $startTimestamp = mktime($hours, $minutes);
                                                list($hours, $minutes) = explode(':', $sleep_time);
                                                $endTimestamp = mktime($hours, $minutes);
                                                $seconds = $endTimestamp - $startTimestamp;
                                                $minutes = ($seconds / 60) % 60;
                                                $hours = floor($seconds / (60 * 60));
                                                $meal_hours_diff = ceil($hours / $tot_meals);
                                                $this_meal_time = date("h:i A", strtotime($t_plan->wake_up_time . ' - ' . $meal_hours_diff . 'hours + 30min'));
                                                ?>
                                                <div class="col-md-12">
                                                    <span style="margin-left: 10px">Meals: <b><?php echo $t_plan->total_meals ?></b></span>
                                                    <div class="margin-top-10"></div>
                                                    <?php for ($i = 1; $i <= $t_plan->total_meals; $i++) { ?>
                                                        <?php
                                                        $t_plan_meal = array_shift($t_plan_meals);
                                                        $saved = is_object($t_plan_meal);
                                                        $this_meal_time = $saved ? $t_plan_meal->meal_time : date('h:i A', strtotime($this_meal_time . ' +' . ($meal_hours_diff) . ' hours'));
                                                        $meal_name = $saved ? $t_plan_meal->meal_name : 'Meal Name';
                                                        $meal_category = $saved ? $t_plan_meal->meal_category : 'BreakFast';
                                                        $meal_proteins = $saved ? $t_plan_meal->meal_proteins : $t_plan->meal_proteins;
                                                        $meal_carbs = $saved ? $t_plan_meal->meal_carbs : $t_plan->meal_carbs;
                                                        $meal_fats = $saved ? $t_plan_meal->meal_fats : $t_plan->meal_fats;
                                                        $meal_carbs_fats = $meal_carbs + $meal_fats;
                                                        $meal_comments = $saved ? $t_plan_meal->meal_comments : "";
                                                        ?>
                                                        <?php if ($saved) { ?><input name="plan_meal_id[<?= $i ?>]" type="hidden" value="<?= $t_plan_meal->id ?>" /><?php } ?>
                                                        <div id="meal_plan_div" data-meal="<?= $i ?>">
                                                            <div class="well" style="border-bottom: solid 3px lightgray">
                                                                <span><i class="fa fa-chevron-circle-right" style="font-size: 17px"></i></span>
                                                                <span style="margin-left: 10px"><strong><?php echo $i ?>:</strong></span>
                                                                <span style="margin-left: 10px"><i class="meal_time"><?php echo $this_meal_time ?></i></span>
                                                                <span style="margin-left: 35px"><i class="meal_name"><?= $meal_name ?></i></span>
                                                                <span style="margin-left: 35px"><b class="meal_category"><?= $meal_category ?></b></span>
                                                                <span style="margin-left: 25px"><i>Proteins:</i> <?php echo $meal_proteins ?>g</span>
                                                                <span style="margin-left: 25px"><i>Fats:</i> <?= $meal_fats ?>g</span>
                                                                <span style="margin-left: 25px"><i>Carbs:</i> <?= $meal_carbs ?>g</span>
                                                                <span style="margin-left: 25px">(<i>Fats/Carbs:</i> <span class="meal_fats_carbs"><?= (($meal_fats / $meal_carbs_fats) * 100) ?>/<?= 100 - (($meal_fats / $meal_carbs_fats) * 100) ?></span>)</span>
                                                            </div>
                                                            <div class="container-fluid">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <table class="table table-bordered">
                                                                            <thead>
                                                                                <tr class="success">
                                                                                    <th>Foods</th>
                                                                                    <th width="20%">Comments</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <th>
                                                                                        <?php $t_plan_meal_foods = $t_plan_meal ? meal_foods::getMealFoods($t_plan_meal->id) : null ?>
                                                                                        <?php if (!empty($t_plan_meal_foods)) { ?>
                                                                                <table class="table table-bordered table-condensed">
                                                                                    <thead>
                                                                                        <tr class="warning">
                                                                                            <th>S.No.</th>
                                                                                            <th>Food Name</th>
                                                                                            <th>Type</th>
                                                                                            <th>Quantity (g)</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php foreach ($t_plan_meal_foods as $k => $t_plan_meal_food) { ?>
                                                                                            <?php
                                                                                            $obj = new food;
                                                                                            $food_info = $obj->getFood($t_plan_meal_food->food_id);
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td><?= $k + 1 ?></td>
                                                                                                <td><?= ucfirst($food_info->name) ?></td>
                                                                                                <td><?= ucfirst($t_plan_meal_food->food_type) ?></td>
                                                                                                <td><?= $t_plan_meal_food->quantity ?></td>
                                                                                            </tr>
                                                                                        <?php } ?>
                                                                                    </tbody>
                                                                                </table>
                                                                            <?php } else { ?>
                                                                                <div class="alert alert-danger"><b>Sorry,</b> No Foods for this Meal.</div>
                                                                            <?php } ?>
                                                                            <?php // pr($t_plan_meal_foods) ?>
                                                                            </th>
                                                                            <th><?= $meal_comments ?></th>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <hr />
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="non_training_day">
                                            <div class="pull-right no-print">
                                                <button id="nt_report_print" class="btn btn-xs btn-primary"><i class="fa fa-print"></i> Print Non-Training Day Plan</button>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-xs-4 col-xs-offset-1">
                                                    <div class="form-horizontal">
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Name :</label>
                                                            <div class="col-xs-6"><?php echo $user->first_name . ' ' . $user->last_name ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Gender :</label>
                                                            <div class="col-xs-6"><?php echo ucfirst($user->gender) ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Contact :</label>
                                                            <div class="col-xs-6"><?php echo $user->contact ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Age :</label>
                                                            <div class="col-xs-6"><?php echo $nt_plan->age ?> Years</div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Proteins:</label>
                                                            <div class="col-xs-6">
                                                                <input type="hidden" id="target_proteins" value="<?php echo $nt_plan->protein_per_day ?>" />
                                                                <?php echo $nt_plan->protein_per_day ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Meals :</label>
                                                            <div class="col-xs-6"><?php echo $nt_plan->total_meals ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-xs-offset-1">
                                                    <div class="form-horizontal">
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Weight :</label>
                                                            <div class="col-xs-6"><?php echo $nt_plan->weight ?> kg</div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Height :</label>
                                                            <div class="col-xs-6"><?php echo $nt_plan->height ?> cm</div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Body Fat :</label>
                                                            <div class="col-xs-6"><?php echo $nt_plan->fats ?> %</div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">BMR :</label>
                                                            <div class="col-xs-6"><?php echo $nt_plan->bmr ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Calories:</label>
                                                            <div class="col-xs-6">
                                                                <input type="hidden" id="target_calories" value="<?php echo $nt_plan->day_calories ?>" />
                                                                <?php echo $nt_plan->day_calories ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Wake Up Time:</label>
                                                            <div class="col-xs-6">
                                                                <?php echo $nt_plan->wake_up_time ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-xs-6" style="margin-top: 0;padding-top: 0">Sleep Time:</label>
                                                            <div class="col-xs-6">
                                                                <?php echo $nt_plan->sleep_time ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr />
                                            <div class="row">
                                                <?php $original_nt_plan_meals = $nt_plan_meals ?>
                                                <?php
//                            pr($nt_plan);
                                                $plan_info_id = $nt_plan->id;
                                                $tot_meals = $nt_plan->total_meals;
                                                $wake_up_time = date("H:i", strtotime($nt_plan->wake_up_time));
                                                $sleep_time = date("H:i", strtotime($nt_plan->sleep_time));
                                                list($hours, $minutes) = explode(':', $wake_up_time);
                                                $startTimestamp = mktime($hours, $minutes);
                                                list($hours, $minutes) = explode(':', $sleep_time);
                                                $endTimestamp = mktime($hours, $minutes);
                                                $seconds = $endTimestamp - $startTimestamp;
                                                $minutes = ($seconds / 60) % 60;
                                                $hours = floor($seconds / (60 * 60));
                                                $meal_hours_diff = ceil($hours / $tot_meals);
                                                $this_meal_time = date("h:i A", strtotime($nt_plan->wake_up_time . ' - ' . $meal_hours_diff . 'hours + 30min'));
                                                ?>
                                                <div class="col-md-12">
                                                    <span style="margin-left: 10px">Meals: <b><?php echo $nt_plan->total_meals ?></b></span>
                                                    <div class="margin-top-10"></div>
                                                    <?php for ($i = 1; $i <= $nt_plan->total_meals; $i++) { ?>
                                                        <?php
                                                        $nt_plan_meal = array_shift($nt_plan_meals);
                                                        $saved = is_object($nt_plan_meal);
                                                        $this_meal_time = $saved ? $nt_plan_meal->meal_time : date('h:i A', strtotime($this_meal_time . ' +' . ($meal_hours_diff) . ' hours'));
                                                        $meal_name = $saved ? $nt_plan_meal->meal_name : 'Meal Name';
                                                        $meal_category = $saved ? $nt_plan_meal->meal_category : 'BreakFast';
                                                        $meal_proteins = $saved ? $nt_plan_meal->meal_proteins : $nt_plan->meal_proteins;
                                                        $meal_carbs = $saved ? $nt_plan_meal->meal_carbs : $nt_plan->meal_carbs;
                                                        $meal_fats = $saved ? $nt_plan_meal->meal_fats : $nt_plan->meal_fats;
                                                        $meal_carbs_fats = $meal_carbs + $meal_fats;
                                                        $meal_comments = $saved ? $nt_plan_meal->meal_comments : "";
                                                        ?>
                                                        <?php if ($saved) { ?><input name="plan_meal_id[<?= $i ?>]" type="hidden" value="<?= $nt_plan_meal->id ?>" /><?php } ?>
                                                        <div id="meal_plan_div" data-meal="<?= $i ?>">
                                                            <div class="well" style="border-bottom: solid 3px lightgray">
                                                                <span><i class="fa fa-chevron-circle-right" style="font-size: 17px"></i></span>
                                                                <span style="margin-left: 10px"><strong><?php echo $i ?>:</strong></span>
                                                                <span style="margin-left: 10px"><i class="meal_time"><?php echo $this_meal_time ?></i></span>
                                                                <span style="margin-left: 35px"><i class="meal_name"><?= $meal_name ?></i></span>
                                                                <span style="margin-left: 35px"><b class="meal_category"><?= $meal_category ?></b></span>
                                                                <span style="margin-left: 25px"><i>Proteins:</i> <?php echo $meal_proteins ?>g</span>
                                                                <span style="margin-left: 25px"><i>Fats:</i> <?= $meal_fats ?>g</span>
                                                                <span style="margin-left: 25px"><i>Carbs:</i> <?= $meal_carbs ?>g</span>
                                                                <span style="margin-left: 25px">(<i>Fats/Carbs:</i> <span class="meal_fats_carbs"><?= (($meal_fats / $meal_carbs_fats) * 100) ?>/<?= 100 - (($meal_fats / $meal_carbs_fats) * 100) ?></span>)</span>
                                                            </div>
                                                            <div class="container-fluid">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <table class="table table-bordered">
                                                                            <thead>
                                                                                <tr class="success">
                                                                                    <th>Foods</th>
                                                                                    <th width="20%">Comments</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <th>
                                                                                        <?php $nt_plan_meal_foods = $nt_plan_meal ? meal_foods::getMealFoods($nt_plan_meal->id) : null ?>
                                                                                        <?php if (!empty($nt_plan_meal_foods)) { ?>
                                                                                <table class="table table-bordered table-condensed">
                                                                                    <thead>
                                                                                        <tr class="warning">
                                                                                            <th>S.No.</th>
                                                                                            <th>Food Name</th>
                                                                                            <th>Type</th>
                                                                                            <th>Quantity (g)</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php foreach ($nt_plan_meal_foods as $k => $nt_plan_meal_food) { ?>
                                                                                            <?php
                                                                                            $obj = new food;
                                                                                            $food_info = $obj->getFood($nt_plan_meal_food->food_id);
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td><?= $k + 1 ?></td>
                                                                                                <td><?= ucfirst($food_info->name) ?></td>
                                                                                                <td><?= ucfirst($nt_plan_meal_food->food_type) ?></td>
                                                                                                <td><?= $nt_plan_meal_food->quantity ?></td>
                                                                                            </tr>
                                                                                        <?php } ?>
                                                                                    </tbody>
                                                                                </table>
                                                                            <?php } else { ?>
                                                                                <div class="alert alert-danger"><b>Sorry,</b> No Foods for this Meal.</div>
                                                                            <?php } ?>
                                                                            <?php // pr($t_plan_meal_foods) ?>
                                                                            </th>
                                                                            <th><?= $meal_comments ?></th>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <hr />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
</div>