<!-- BEGIN PAGE SIDEBAR -->
<div class="page-sidebar">
    <nav class="navbar" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="clearfix">
            <!-- Toggle Button -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-siderbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="toggle-icon">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </span> 
            </button>
            <!-- End Toggle Button -->
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="nav-collapse collapse navbar-collapse page-siderbar-collapse">
            <ul class="nav navbar-nav">
                <li class="<?= ($section == 'list') ? 'active' : '' ?>">
                    <a href="<?= make_admin_url('plan', 'list', 'list') ?>">
                        <i class="fa fa-list "></i>
                        View All Plans
                    </a>
                </li>
                <li class='<?= ($section == 'insert') ? 'active' : '' ?>'>
                    <a href="<?= make_admin_url('plan', 'insert', 'insert') ?>">
                        <i class="fa fa-plus "></i>
                        Add New Plan 
                    </a>
                </li>
            </ul>
        </div>
        <?php if ($section == 't_plan' || $section == 'nt_plan' || $section == 'update') { ?>
            <hr />
            <?php if (plan_meals::hasBothPlans($id)) { ?>
                <div class="alert alert-success">
                    <center><b><i class="fa fa-file-text"></i> Reports</b></center><hr style="margin: 3px 0" />
                    You have made plans for both Training and Non-Training Days for this User. Click <a href="<?php echo make_admin_url('plan', 'list', 'report', 'id=' . $id) ?>">here</a> to view the reports.
                </div>
            <?php } else { ?>
                <div class="alert alert-warning">
                    <center><b><i class="fa fa-file-text"></i> Reports</b></center><hr style="margin: 3px 0" />
                    Meal plan reports can only be generated after making the plans for Training and Non-Training days.
                </div>
            <?php } ?>
        <?php } ?>
    </nav>
</div>
<!-- END PAGE SIDEBAR -->