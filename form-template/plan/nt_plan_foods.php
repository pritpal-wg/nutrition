<?php
$nt_plan_meal_foods = $nt_plan_meal ? meal_foods::getMealFoods($nt_plan_meal->id) : null;
$nt_plan_meal_food = null;
if ($nt_plan_meal_foods) {
    $nt_plan_meal_food = array_shift($nt_plan_meal_foods);
}
?>
<table class="table table-condensed table-bordered">
    <thead>
        <tr class="info">
            <th style="width: 40%" colspan="2">Food</th>
            <th style="width: 12%">Quantity</th>
            <th>Unit</th>
            <?php foreach ($food_types as $food_type) { ?>
                <th>
                    <?= ucfirst($food_type) ?>s
                </th>
            <?php } ?>
        </tr>
    </thead>
    <tbody class="foods_container">
        <tr data-for="food">
            <td style="width: 80px">
                <button type="button" class="btn btn-xs btn-success add_food pull-right"><i class="fa fa-plus"></i></button>
            </td>
            <td>
                <select class="form-control" data-toggle="chosen" id="food_select" name="meal_foods[<?= $i ?>][food_id][]" style="margin-top: 5px">
                    <option value="">Select Food</option>
                    <?php foreach ($foods as $food) { ?>
                        <option value="<?php echo $food->id ?>"<?= $nt_plan_meal_food && $nt_plan_meal_food->food_id == $food->id ? ' selected' : '' ?>><?php echo ucfirst($food->name) ?></option>
                    <?php } ?>
                </select>
            </td>
            <td>
                <input type="number" class="form-control" name="meal_foods[<?= $i ?>][quantity][]" placeholder="Quantity" style="width: 100%;padding: 0 3px;height: 24px" step="any" min="0.1" id="food_quantity" value="<?= $nt_plan_meal_food ? $nt_plan_meal_food->quantity : '' ?>" />
            </td>
            <td data-id="unit" style="font-weight: 400">-</td>
            <?php foreach ($food_types as $food_type) { ?>
                <td data-food_type="<?= $food_type ?>" style="font-weight: 400">0</td>
            <?php } ?>
        </tr>
        <?php if (is_array($nt_plan_meal_foods) && !empty($nt_plan_meal_foods)) { ?>
            <?php foreach ($nt_plan_meal_foods as $nt_plan_meal_food) { ?>
                <tr data-for="food">
                    <td>
                        <div class="btn-group pull-right" role="group" aria-label="...">
                            <button type="button" class="btn btn-xs btn-success add_food" style="margin-right: 0"><i class="fa fa-plus"></i></button>
                            <button type="button" class="btn btn-xs btn-danger remove_food" style="margin-right: 0"><i class="fa fa-times"></i></button>
                        </div>
                    </td>
                    <td>
                        <select class="form-control" data-toggle="chosen" id="food_select" name="meal_foods[<?= $i ?>][food_id][]" style="margin-top: 5px">
                            <option value="">Select Food</option>
                            <?php foreach ($foods as $food) { ?>
                                <option value="<?php echo $food->id ?>"<?= $nt_plan_meal_food && $nt_plan_meal_food->food_id == $food->id ? ' selected' : '' ?>><?php echo ucfirst($food->name) ?></option>
                            <?php } ?>
                        </select>
                    </td>
                    <td>
                        <input type="number" class="form-control" name="meal_foods[<?= $i ?>][quantity][]" placeholder="Quantity" style="width: 100%;padding: 0 3px;height: 24px" step="any" min="0.1" id="food_quantity" value="<?= $nt_plan_meal_food ? $nt_plan_meal_food->quantity : '' ?>" />
                    </td>
                    <td data-id="unit" style="font-weight: 400">-</td>
                    <?php foreach ($food_types as $food_type) { ?>
                        <td data-food_type="<?= $food_type ?>" style="font-weight: 400">0</td>
                    <?php } ?>
                </tr>
            <?php } ?>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <th colspan="4">Total</th>
            <!--<th data-id="total_quantity"></th>-->
            <?php foreach ($food_types as $food_type) { ?>
                <th data-id="total_micro" data-food_type="<?= $food_type ?>">
                    <span id="meal_calculated_<?= $food_type ?>">0</span>
                    /
                    <span class="meal_<?= $food_type ?>s"><?php echo ${meal_ . $food_type . s} ?></span> <span title="Calories">C.</span>
                </th>
            <?php } ?>
        </tr>
    </tfoot>
</table>