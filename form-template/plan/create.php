<!-- Page Content Start -->
<div class="page-content">
    <!-- BEGIN BREADCRUMBS -->
    <div class="breadcrumbs">
        <h1>Nutrition Plans</h1>
        <ol class="breadcrumb">
            <li><a href="<?= make_admin_url('home') ?>">Home</a></li>
            <li><a href="<?= make_admin_url('plan', 'list', 'list') ?>">Nutrition Plans</a></li>
            <li class="active">New Plan</li>
        </ol>
    </div>
    <!-- END BREADCRUMBS -->
    <?php display_message(1) ?>
    <!-- Left Bar Sortcut-->
    <?php include_once(DIR_FS_SITE . '/form-template/' . $modName . '/shortcut.php'); ?>
    <!-- BEGIN PAGE CONTAINER -->
    <div class="page-container">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box sky-blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="fa fa-list" style="color: #fff"></i>New Plan</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form form-horizontal validation" method="POST" enctype="multipart/form-data">
                            <div class="form-body">
                                <h3 style="margin-top: 0"><center>Client Information</center></h3>
                                <hr />
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="col-md-3 control-label padding-right-none">Select Client:</label>
                                            <div class="col-md-7">
                                                <select class="form-control plan_user validate[required]" name="user_id" id='user_id'>
                                                    <option value=''>Select Client</option>
                                                    <?php
                                                    if (!empty($allUser)) {
                                                        foreach ($allUser as $key => $val) {
                                                            ?>	
                                                            <option value='<?= $val->id ?>'<?php echo (isset($plan_info) && $plan_info[0]->user_id === $val->id) ? ' selected' : ''; ?>><?= ucfirst($val->first_name . ' ' . $val->last_name) ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>		
                                        <div class="form-group">
                                            <label for="weight" class="col-md-3 control-label padding-right-none">Weight:</label>
                                            <div class="col-md-3 padding-right-none">
                                                <input type="text" class="form-control validate[required] userPlanInfo" readonly id="weight" name="weight" placeholder="Enter Weight">
                                            </div>
                                            <div class="col-md-2 padding-left-none">
                                                <select class="form-control userPlanWeightType" id="weight_type" name="weight_type" placeholder="Enter Weight">
                                                    <option value='kg'>kg</option>
                                                    <option value='lbs'>lbs</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="height" class="col-md-3 control-label padding-right-none">Height (cm):</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control validate[required,custom[number]] userPlanInfo" readonly id="height" name="height" placeholder="Enter Height">
                                            </div>
                                        </div>	
                                        <div class="form-group">
                                            <label for="fat" class="col-md-3 control-label padding-right-none">Body Fat (%):</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control validate[required,custom[number]] userPlanInfo" readonly id="fat" name="fat" placeholder="Body Fat">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="method" class="col-md-3 control-label padding-right-none">Method Used:</label>
                                            <div class="col-md-7">
                                                <select class="form-control validate[required]" name="method" id="method">
                                                    <?php
                                                    if (!empty($methodList)) {
                                                        foreach ($methodList as $key => $val) {
                                                            ?>	
                                                            <option value='<?= $val->id ?>'><?= ucfirst($val->name) ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="bmr" class="col-md-3 control-label padding-right-none">BMR</label>
                                            <div class="col-md-7">
                                                <input type="bmr" class="form-control validate[required]" readonly id="bmr" name="bmr" placeholder="bmr" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label padding-right-none">Activity Factor:</label>
                                            <div class="col-md-7">
                                                <select class="form-control validate[required]" name="activity_factor" id="activity_factor">
                                                    <option value="">Select Activity Factor</option>
                                                    <?php
                                                    if (!empty($allFactor)) {
                                                        foreach ($allFactor as $key => $val) {
                                                            ?>
                                                            <option value="<?php echo $val->id ?>"><?php echo ucfirst($val->name) ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="show_plan_user_info" style="min-height: 200px">
                                            <div style="padding-top: 70px;text-align: center">
                                                <h2>Select a Client from the list.</h2>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="form-group">
                                                <label for="training_per_week" class="col-md-4 control-label padding-right-none">Trainings Per Week:</label>
                                                <div class="col-md-6">
                                                    <input id="training_per_week" type="text" class="form-control validate[required,custom[number]]" name="training_per_week" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-md-4 control-label padding-right-none">Cardio Trainings Per Week:</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control validate[required,custom[number]]" name="cardio_per_week" id="cardio_per_week" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="total_meals" class="col-md-4 control-label padding-right-none">Number Of Meals:</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control validate[required,custom[integer]]" name="total_meals" id="total_meals" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr />
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">Protein Requirements</h4>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="ppkg" class="col-md-4 control-label padding-right-none">Protein(g/kg)</label>
                                                    <div class="col-md-7">
                                                        <input type="number" class="form-control validate[required]" id="ppkg" name="ppkg" placeholder="Protein" step="any" min="0.01" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="protein_per_day" class="col-md-4 control-label padding-right-none">Total Protein(g):</label>
                                                    <div class="col-md-7">
                                                        <input type="text" class="form-control" name="protein_per_day" id="protein_per_day" readonly />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="protein_calories" class="col-md-4 control-label padding-right-none">Calories From Protein:</label>
                                                    <div class="col-md-7">
                                                        <input type="text" class="form-control" name="protein_calories" id="protein_calories" readonly />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">Macronutrient Distribution</h4>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="" class="col-md-3 control-label padding-right-none">Fats:</label>
                                                    <div class="col-md-7 padding-left-none" for='fat'>
                                                        <select class="form-control validate[required]" name="fats" id='fats'>
                                                            <?php for ($i = 1; $i < 100; $i ++) { ?>
                                                                <option value="<?= $i ?>"<?= $i == 60 ? ' selected' : '' ?>><?= $i ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-3 control-label padding-right-none">Carbs:</label>
                                                    <div class="col-md-7 padding-right-none" for='carbs'>
                                                        <select class="form-control validate[required]" name="carbs" id='carbs'>
                                                            <?php for ($i = 1; $i < 100; $i++) { ?>
                                                                <option value="<?= $i ?>"<?= $i == 40 ? ' selected' : '' ?>><?= $i ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">Recommended Calorie Modification</h4>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="calorie_profile" class="col-md-4 control-label padding-right-none">Profile</label>
                                                    <div class="col-md-7">
                                                        <select name="calorie_profile" id="calorie_profile" class="form-control">
                                                            <?php foreach ($calorie_profiles as $calorie_profile => $calorie_profile_info) { ?>
                                                                <option value="<?= $calorie_profile ?>"><?php echo $calorie_profile_info['label'] ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label padding-right-none">Percentage:</label>
                                                    <div class="col-md-7">
                                                        <input type="number" class="form-control validate[required,custom[number]]" name="calorie_profile_percent" id="calorie_profile_percent" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">Extras</h4>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="" class="col-md-4 control-label padding-right-none" title="Training Day vs Non Training Day Differential">Differential (%):</label>
                                                    <div class="col-md-7">
                                                        <input id="differential" type="number" class="form-control" name="differential" value="90" max="99" min="0" title="This value represents the fraction of the Training Day Recommendation that the Non-Training Day Recommendation in the first section of the report, and on the guidance provided at the top of the Non-Training Day Diet Plan tab display." />
                                                    </div>
                                                </div>
                                                <div class="form-group" hidden>
                                                    <label for="" class="col-md-4 control-label padding-right-none">Regular Calories/Day:</label>
                                                    <div class="col-md-7">
                                                        <input id="regular_calories" type="text" class="form-control" name="regular_calories" title="Enter value in this field only for patients who are consuming too many calories." />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4 control-label padding-right-none">Wake Up Time:</label>
                                                    <div class="col-md-7">
                                                        <input type="text" class="form-control validate[required] timepicker timepicker-no-seconds" name="wake_up_time" id="wake_up_time" value="7:00 AM" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4 control-label padding-right-none">Training Time:</label>
                                                    <div class="col-md-7">
                                                        <input type="text" class="form-control validate[required] timepicker timepicker-no-seconds"  name="training_time" id="training_time" value="10:00 AM" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4 control-label padding-right-none">Sleep Time:</label>
                                                    <div class="col-md-7">
                                                        <input type="text" class="form-control validate[required] timepicker timepicker-no-seconds"  name="sleep_time" id="sleep_time" value="7:00 PM" />
                                                    </div>
                                                </div>
                                                <div class="form-group" hidden>
                                                    <label for="day_calories" class="col-md-5 control-label padding-right-none padding-left-none">Calories Per Day:</label>
                                                    <div class="col-md-7">
                                                        <input type="text" class="form-control validate[required,custom[number]]" name="day_calories" id="day_calories" value="1" />
                                                    </div>
                                                </div>
                                                <div class="form-group" hidden>
                                                    <label for="target_calories" class="col-md-4 control-label padding-right-none">Target Calories:</label>
                                                    <div class="col-md-7">
                                                        <input type="text" class="form-control validate[required,custom[number]]" name="target_calories" id="target_calories" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h3><center>Calculated Values</center></h3>
                                <hr />
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Basal Metabolic Rate</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row-fluid">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                                <tr class="warning">
                                                                    <th>Method</th>
                                                                    <th>Calories/day</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="bmr_info_body">
                                                                <?php foreach ($methodList as $k => $method) { ?>
                                                                    <tr data-for="bmr_info" data-method_id="<?= $method->id ?>">
                                                                        <td><?= ucfirst($method->name) ?></td>
                                                                        <td id="_calories">0</td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Lean Body Mass (kg)</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row-fluid">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5">Lean Body Mass (kg):</label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control" id="lean_body_mass" readonly />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">BMI:</label>
                                                        <div class="col-md-7">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="bmi" name="bmi" readonly />
                                                                <span class="input-group-addon">kg/m<sup>2</sup></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Lean BMI:</label>
                                                        <div class="col-md-7">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="lean_bmi" name="lean_bmi" readonly/>
                                                                <span class="input-group-addon">kg/m<sup>2</sup></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Estimated Daily Nutrient Requirements (Calories)</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row-fluid">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered" style="margin-bottom: 0">
                                                            <thead>
                                                                <tr class="warning">
                                                                    <th>Method</th>
                                                                    <th style="width: 15%">Total</th>
                                                                    <th style="width: 15%">Protein</th>
                                                                    <th style="width: 15%">Fat</th>
                                                                    <th style="width: 15%">Carb.</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="estimated_daily_cals">
                                                                <?php foreach ($methodList as $k => $method) { ?>
                                                                    <tr data-for="estimated_daily_cal" data-method_id="<?= $method->id ?>">
                                                                        <td><?= ucfirst($method->name) ?></td>
                                                                        <td id="_total">0</td>
                                                                        <td id="_protein">0</td>
                                                                        <td id="_fat">0</td>
                                                                        <td id="_carb">0</td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Recommended Daily Nutrient Requirements</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row-fluid">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered" style="margin-bottom: 0">
                                                            <thead>
                                                                <tr class="warning">
                                                                    <th>Method</th>
                                                                    <th style="width: 15%">Total</th>
                                                                    <th style="width: 15%">Protein</th>
                                                                    <th style="width: 15%">Fat</th>
                                                                    <th style="width: 15%">Carb.</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="recommended_daily_cals">
                                                                <?php foreach ($methodList as $k => $method) { ?>
                                                                    <tr data-for="recommended_daily_cal" data-method_id="<?= $method->id ?>">
                                                                        <td><?= ucfirst($method->name) ?></td>
                                                                        <td id="_total">0</td>
                                                                        <td id="_protein">0</td>
                                                                        <td id="_fat">0</td>
                                                                        <td id="_carb">0</td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-2 col-md-3">
                                        <button type="submit" name='submit_t' class="btn btn-block btn-primary"><i class="fa fa-calendar"></i> Training Day Plan</button>
                                    </div>
                                    <div class="col-md-offset-2 col-md-3">
                                        <button type="submit" name='submit_nt' class="btn btn-block btn-primary"><i class="fa fa-calendar-o"></i> Non-Training Day Plan</button>
                                    </div>
                                </div>
                                <hr style="margin: 5px" />
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-4">
                                        <button type="submit" name="submit" class="btn btn-block btn-default"><i class="fa fa-save"></i> Save Data</button>
                                    </div>
                                </div>
                            </div>
                            <div class='clear'></div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
</div>
<script>
    var calorie_profiles = JSON.parse('<?php echo json_encode($calorie_profiles) ?>');
    var activity_factors = JSON.parse('<?php echo json_encode($allFactor) ?>');
//    console.log(activity_factors);
</script>