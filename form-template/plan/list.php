<!-- Page Content Start -->
<div class="page-content">
    <!-- BEGIN BREADCRUMBS -->
    <div class="breadcrumbs">
        <h1>Nutrition Plans</h1>
        <ol class="breadcrumb">
            <li><a href="<?= make_admin_url('home') ?>">Home</a></li>
            <li class="active">Nutrition Plans</li>
        </ol>
    </div>
    <!-- END BREADCRUMBS -->
    <?php display_message(1) ?>
    <!-- Left Bar Sortcut-->
    <?php include_once(DIR_FS_SITE . '/form-template/' . $modName . '/shortcut.php'); ?>
    <!-- BEGIN PAGE CONTAINER -->
    <div class="page-container">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box sky-blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="fa fa-list" style="color: #fff"></i>Plans List</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dataTable no-footer" role="grid" id="sample_1">
                            <thead>
                                <tr>
                                    <th style="width:65px">Sr. No.</th>
                                    <th class="hidden-480">User</th>
                                    <th class="hidden-480 text-center">Created On</th>
                                    <!--<th class="hidden-480 sorting_disabled text-center">Status</th>-->
                                    <th style='text-align:right;'>Action</th>
                                </tr>
                            </thead>
                            <? if (!empty($record)): ?>
                                <tbody>
                                    <?php
                                    $sr = 1;
                                    foreach ($record as $key => $object):
                                        $user_obj = new user;
                                        $user = $user_obj->getUser($object->user_id);
//                                        pr($user);
                                        ?>
                                        <tr>
                                            <td><?= $sr ?>.</td>
                                            <td><?= $user->first_name . ' ' . $user->last_name ?></td>
                                            <td class='text-center'><?= date('l, M-d-Y', strtotime($object->date_add)) . ' at ' . date('H:i', strtotime($object->date_add)) ?></td>
                                            <!--<td class='text-center'><?= ($object->is_active == '1') ? 'Active' : 'Not-Active' ?></td>-->
                                            <td class='right'>
                                                <?php if (plan_meals::hasBothPlans($object->id)) { ?>
                                                    <a class="btn mini yellow icn-only tooltips margin-left-none margin-right-none" href="<?php echo make_admin_url('plan', 'list', 'report', 'id=' . $object->id) ?>" title="View Report"><i class="fa fa-file-text-o"></i></a>
                                                <?php } ?>
                                                <a class="btn mini blue icn-only tooltips margin-left-none margin-right-none" href="<?php echo make_admin_url('plan', 'update', 'update', 'id=' . $object->id) ?>" title="Edit Plan"><i class="fa fa-edit"></i></a>
                                                <a class="btn mini red icn-only tooltips margin-left-none margin-right-none" href="<?php echo make_admin_url('plan', 'delete', 'delete', 'id=' . $object->id) ?>" onclick="return confirm('This will delete the Training and Non-Training day Plan.\nAre you sure? You want to do this.');" title="Delete Plan"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                        $sr++;
                                    endforeach;
                                    ?>
                                </tbody>
                            <?php endif; ?>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
</div>
<!-- END PAGE CONTAINER -->
</div>
<!-- PAGE CONTENT END -->