<?php
$user_id = ($_POST['user_id']) ? $_POST['user_id'] : '0';

#user Info
$QueryObj = new user;
$user = $QueryObj->getUser($user_id);


if (is_object($user)):
    ?>
    <div class="row">
        <div class="col-md-8 col-md-offset-1" style="padding: 15px">
            <h4 class="ajax-form-group green bold page-header-fixed">Quick User Information:</h4>
            <hr style="margin: 5px 0" />
            <div class="ajax-form-group">
                <label class="col-md-4 padding-right-none green">First Name:</label>
                <div class="col-md-7">
                    <?= ucfirst($user->first_name) ?>
                </div>
                <div class='clear'></div>
            </div>	
            <div class="ajax-form-group">
                <label class="col-md-4 padding-right-none green">Last Name:</label>
                <div class="col-md-7">
                    <?= $user->last_name ?>
                </div>
                <div class='clear'></div>
            </div>	
            <div class="ajax-form-group">
                <label class="col-md-4 padding-right-none green">Gender:</label>
                <div class="col-md-7">
                    <?= ucfirst($user->gender) ?>
                </div>
                <div class='clear'></div>
            </div>		
            <div class="ajax-form-group">
                <label class="col-md-4 padding-right-none green">Email:</label>
                <div class="col-md-7">
                    <?= $user->email ?>
                </div>
                <div class='clear'></div>
            </div>	
            <div class="ajax-form-group">
                <label class="col-md-4 padding-right-none green">Contact No:</label>
                <div class="col-md-7">
                    <?= $user->contact ?>
                </div>
                <div class='clear'></div>
            </div>	
            <div class="ajax-form-group">
                <label class="col-md-4 padding-right-none green">Age:</label>
                <div class="col-md-7">
                    <?= show_age($user->date_of_birth) ?>
                </div>
                <div class='clear'></div>
            </div>	
            <span class='sex_name hidden'><?= $user->gender ?></span>
        </div>
    </div>
    <hr style="margin: 15px" />
<? endif; ?>