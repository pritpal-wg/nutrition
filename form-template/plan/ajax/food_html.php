<?php
$obj = new food;
$obj->Where = "WHERE is_active='1'";
$foods = $obj->ListOfAllRecords('object');
$food_types = array(
    'protein',
    'fat',
    'carb',
);
$i = $meal_num;
?>
<tr data-for="food">
    <td>
        <div class="btn-group pull-right" role="group" aria-label="...">
            <button type="button" class="btn btn-xs btn-success add_food" style="margin-right: 0"><i class="fa fa-plus"></i></button>
            <button type="button" class="btn btn-xs btn-danger remove_food" style="margin-right: 0"><i class="fa fa-times"></i></button>
        </div>
    </td>
    <td>
        <select class="form-control" data-toggle="chosen" id="food_select" name="meal_foods[<?= $i ?>][food_id][]" style="width: 230px;margin-top: 5px">
            <option value="">Select Food</option>
            <?php foreach ($foods as $food) { ?>
                <option value="<?php echo $food->id ?>"><?php echo ucfirst($food->name) ?></option>
            <?php } ?>
        </select>
    </td>
    <td>
        <input type="number" class="form-control" name="meal_foods[<?= $i ?>][quantity][]" placeholder="Quantity" style="width: 100%;padding: 0 3px;height: 24px" step="any" min="0.1" id="food_quantity" />
    </td>
    <td data-id="unit" style="font-weight: 400">-</td>
    <?php foreach ($food_types as $food_type) { ?>
        <td data-food_type="<?= $food_type ?>" style="font-weight: 400">0</td>
    <?php } ?>
</tr>