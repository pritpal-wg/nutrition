<!-- Page Content Start -->
<div class="page-content">
    <!-- BEGIN BREADCRUMBS -->
    <div class="breadcrumbs">
        <h1>Manage Foods</h1>
        <ol class="breadcrumb">
            <li><a href="<?= make_admin_url('home') ?>">Home</a></li>
            <li class="active">View Food Categories</li>
        </ol>
    </div>
    <!-- END BREADCRUMBS -->

    <?php
    /* display message */
    display_message(1);
    ?>	

    <!-- Left Bar Sortcut-->
    <?php include_once(DIR_FS_SITE . '/form-template/food/shortcut.php'); ?>  


    <!-- BEGIN PAGE CONTAINER -->
    <div class="page-container">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box sky-blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i>Food Categories</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dataTable no-footer" id="sample_1" role="grid">
                            <thead>
                                <tr>
                                    <th style="width:65px;">Sr. No.</th>
                                    <th class="hidden-480 sorting_disabled">Category Name</th>
                                    <th class="hidden-480 sorting_disabled">Parent Category</th>
                                    <th class="hidden-480 sorting_disabled text-center">Status</th>
                                    <th style='text-align:right;'>Action</th>
                                </tr>
                            </thead>
                            <? if (!empty($record)): ?>
                                <tbody>
                                    <?php
                                    $sr = 1;
                                    foreach ($record as $key => $object):
                                        $parent_cat = (new foodCat)->getField($object->parent_id, 'name', true);
                                        ?>
                                        <tr>
                                            <td><?= $sr ?>.</td>
                                            <td><?= ucfirst($object->name) ?></td>
                                            <td><?= $parent_cat ? '<a href="' . make_admin_url('food_cat', 'update', 'update', 'id=' . ((new foodCat)->getField($object->parent_id, 'id', true))) . '">' . $parent_cat . '</a>' : '<small style="color: gray">Top Category</small>' ?></td>
                                            <td class='text-center'><?= ($object->is_active == '1') ? 'Active' : 'Not-Active' ?></td>
                                            <td class='right'>
                                                <a class="btn mini blue icn-only tooltips margin-left-none margin-right-none" href="<?php echo make_admin_url('food_cat', 'update', 'update', 'id=' . $object->id) ?>" title="click here to edit this record"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                                <a class="btn mini red icn-only tooltips margin-left-none margin-right-none" href="<?php echo make_admin_url('food_cat', 'delete', 'delete', 'id=' . $object->id . '&delete=1') ?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                        $sr++;
                                    endforeach;
                                    ?>
                                </tbody>
                            <?php endif; ?>  
                        </table>
                    </div>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>		

    </div>

</div>
<!-- END PAGE CONTAINER -->
</div>
<!-- PAGE CONTENT END -->