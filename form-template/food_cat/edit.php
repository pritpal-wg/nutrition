<!-- Page Content Start -->
<div class="page-content">
    <!-- BEGIN BREADCRUMBS -->
    <div class="breadcrumbs">
        <h1>Manage Foods</h1>
        <ol class="breadcrumb">
            <li><a href="<?= make_admin_url('home') ?>">Home</a></li>
            <li><a href="<?= make_admin_url('food_cat') ?>">View List</a></li>
            <li class="active">Edit</li>
        </ol>
    </div>
    <!-- END BREADCRUMBS -->
    <?php
    /* display message */
    display_message(1);
    ?>	
    <!-- Left Bar Sortcut-->
    <?php include_once(DIR_FS_SITE . '/form-template/food/shortcut.php'); ?>  
    <!-- BEGIN PAGE CONTAINER -->
    <div class="page-container">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box sky-blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="fa fa-plus"></i>Edit Food Category</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form form-horizontal validation" action="<?php echo make_admin_url('food_cat', 'update', 'update') ?>" method="POST" enctype="multipart/form-data" >
                            <div class="form-body">
                                <div class="form-group">
                                    <label for="name" class="col-md-2 control-label padding-right-none">Category Name</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control validate[required]" id="name" name="name" value='<?= $food->name ?>' placeholder="Category Name">
                                    </div>
                                </div>
                                <?php if (!empty($top_cats)) { ?>
                                    <div class="form-group">
                                        <label for="parent_id" class="col-md-2 control-label padding-right-none">Parent Category</label>
                                        <div class="col-md-4">
                                            <select name="parent_id" class="form-control">
                                                <option value="0">--Top Category--</option>
                                                <?php echo cats_loop($top_cats, '', $food->parent_id, $food->id) ?>
                                                <?php // foreach ($top_cats as $cat) { ?>
                                                    <!--<option value="<?= $cat->id ?>"<?= $cat->id == $food->parent_id ? ' selected' : '' ?>><?= $cat->name ?></option>-->
                                                <?php // } ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="form-group">
                                    <label for="description" class="col-md-2 control-label padding-right-none">Description</label>
                                    <div class="col-md-4">
                                        <textarea class="form-control" rows="3" name='description' placeholder="Description"><?= $food->description ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-md-2 control-label padding-right-none">Active</label>
                                    <div class="col-md-4 padding-top-checkbox">
                                        <input type="checkbox" name='is_active' <?= ($food->is_active == '1') ? 'checked' : '' ?> value='1'>
                                        <input type='hidden' name='id' value="<?= $food->id ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="col-md-offset-2 col-md-10">
                                    <a href="<?php echo make_admin_url('food', 'list', 'list'); ?>" class="btn btn-default" name="cancel" > Cancel</a>
                                    <button type="submit" name='submit' class="btn blue">Submit</button>
                                </div>
                            </div>
                            <div class='clear'></div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
</div>
<!-- END PAGE CONTAINER -->
</div>
<!-- PAGE CONTENT END -->