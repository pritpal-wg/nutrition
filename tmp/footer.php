<!-- Copyright BEGIN -->
<p class="copyright">
    <?php echo date('Y') ?> &copy; <?php echo SITE_NAME ?> - Management Panel.
</p>
<!-- Copyright END -->
</div>
</div>
<!-- END MAIN LAYOUT -->
<a href="#index" class="go2top"><i class="icon-arrow-up"></i></a>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->

<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="assets/global/scripts/jquery.print.js"></script>

<!-- Validation engine -->
<script type="text/javascript" charset="utf-8" src="assets/global/plugins/validation/languages/jquery.validationEngine-en.js" ></script>
<script type="text/javascript" charset="utf-8" src="assets/global/plugins/validation/jquery.validationEngine.js"></script>

<!-- Date Picker -->
<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<!-- BEGIN Global SCRIPTS -->
<script src="assets/global/scripts/chosen.js" type="text/javascript"></script>
<script src="assets/global/scripts/toastr.min.js" type="text/javascript"></script>
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/app/app_layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/app/app_layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/app/app_layout/scripts/index.js" type="text/javascript"></script>
<script src="assets/app/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="assets/app/pages/scripts/table-advanced.js"></script>
<script src="assets/app/pages/scripts/components-pickers.js"></script>
<script src="assets/app/app_layout/scripts/plan.js"></script>
<script src="assets/app/app_layout/scripts/custom.js"></script>
<script>
    jQuery(document).ready(function () {
        jQuery('.validation').validationEngine();
//        Metronic.init(); // init metronic core components
        //Layout.init(); // init current layout		
        ComponentsPickers.init();
        TableAdvanced.init();
    });
</script>


</body>
<!-- END BODY -->
</html>