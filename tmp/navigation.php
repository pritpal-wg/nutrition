					<!-- Collect the nav links, forms, and other content for toggling -->
	                <div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
	                    <ul class="nav navbar-nav text-uppercase">
<!--	                        <li class="dropdown-fw <?=($Page=='home')?'open':''?>">
	                            <a href='<?=make_admin_url('home');?>'>
	                            	Dashboard
	                            </a>
	                        </li>-->
	                        <li class="dropdown-fw <?=($Page=='user')?'open':''?>">
	                            <a href='<?=make_admin_url('user');?>'>
	                            	Users
	                            </a>
			                </li>
	                        <li class="dropdown-fw <?=($Page=='plan')?'open':''?>">
	                            <a href='<?=make_admin_url('plan');?>'>
	                            	Nutrition Plans
	                            </a>
			                </li>
	                        <li class="dropdown-fw <?=($Page=='food' || $Page=='food_cat')?'open':''?>">
	                            <a href='<?=make_admin_url('food');?>'>
	                            	Foods
	                            </a>
			                </li>
	                        <li class="dropdown-fw <?=($Page=='application')?'open':''?>">
	                            <a href='<?=make_admin_url('application');?>'>
	                            	Application Settings
	                            </a>
			                </li> 							
	                        <li class="dropdown-fw <?=($Page=='setting')?'open':''?>">
	                            <a href='<?=make_admin_url('setting');?>'>
	                            	Website Settings
	                            </a>
			                </li>                    
	                    </ul>
	                </div>
	                <!-- END NAVBAR COLLAPSE -->