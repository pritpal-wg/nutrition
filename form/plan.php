<!-- section coding -->
<?php
/* sections */
switch ($section):
    case 'list':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/list.php');
        break;
    case 'update':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/edit.php');
        break;
    case 'insert':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/create.php');
        break;
    case 'view':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/view.php');
        break;
    case 'trash':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/trash.php');
        break;
    case 't_plan':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/t_plan.php');
        break;
    case 'nt_plan':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/nt_plan.php');
        break;
    case 'report':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/report.php');
        break;
    default:break;
endswitch;
?>




