<?php

require_once("include/config/config.php");
include DIR_FS_SITE . 'include/class/adminlog.php';
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/foodClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/otherSettingClass.php');

$include_fucntions = array('video', 'date', 'email', 'file_handling', 'http',
    'image_manipulation', 'paging', 'news', 'testimonials',
    'recaptchalib', 'content', 'footer', 'gallery', 'users');

include_functions($include_fucntions);

if (!$admin_user->is_logged_in()):
    Redirect(DIR_WS_SITE . '/index.php');
endif;

$PageAccess = '*';

$Page = isset($_GET['Page']) ? $_GET['Page'] : "home";
if ($Page != '' && file_exists(DIR_FS_SITE . '/script/' . $Page . '.php')):
    if ($PageAccess == '*' || array_key_exists($Page, $PageAccess)):

        if ($PageAccess != '*'):
            $action = isset($_GET['action']) ? $_GET['action'] : "list";


            if (isset($PageAccess[$Page][$action])):
                include(DIR_FS_SITE . '/script/' . $Page . '.php');
            else:
            //echo"<p style='height:250px;'><br><br><font size='3'><b>Welcome ".$admin_user->get_username()."!</b></font><br><br>";
            //echo "You do not have the permission to access this page.</p>";
            endif;
        else:
            include(DIR_FS_SITE . '/script/' . $Page . '.php');
        endif;
    endif;
endif;
include_once(DIR_FS_SITE . '/tmp/header.php');


/* Udpate admin log  - only update operations are logged */
if (count($_POST)) {
    $adlog = new adminlog();
    $adlog->setMsg('Action ' . $action . ' performed on ' . $id . ' in ' . $Page . ' module');
}

if ($Page != "") {
    if (file_exists(DIR_FS_SITE . '/form/' . $Page . ".php")):
        if ($PageAccess == '*' || array_key_exists($Page, $PageAccess)):
            if ($PageAccess != '*'):
                if (isset($PageAccess[$Page][$action])):
                    require_once(DIR_FS_SITE . '/form/' . $Page . ".php");
                else:
                    echo"<p style='height:250px;'><br><br><font size='3'><b>Welcome " . $admin_user->get_username() . "!</b></font><br><br>";
                    echo "You do not have the permission to access this page.</p>";
                endif;
            else:
                require_once(DIR_FS_SITE . '/form/' . $Page . ".php");
            endif;
        else:
            echo"<p style='height:250px;'><br><br><font size='3'><b>Welcome " . $admin_user->get_username() . "!</b></font><br><br>";
            echo "You do not have the permission to access this page.</p>";
        endif;
    else:
        if ($PageAccess == '*' || array_key_exists($Page, $PageAccess)):
            if ($PageAccess != '*'):

                if (isset($PageAccess[$Page][$action])):
                    require_once(DIR_FS_SITE . '/form/default.php');
                else:
                    echo"<p style='height:250px;'><br><br><font size='3'><b>Welcome " . $admin_user->get_username() . "!</b></font><br><br>";
                    echo "You do not have the permission to access this page.</p>";
                endif;
            else:
                require_once(DIR_FS_SITE . '/form/default.php');
            endif;
        else:
            echo "<p style='height:250px;'><font size='3'><br><br><b>You do not have the permission to access this page.</b></font></p>";
        endif;
    endif;
}

require_once(DIR_FS_SITE . '/tmp/' . "footer.php");
?>