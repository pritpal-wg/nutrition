(function ($) {
    $.fn.hasAttr = function (e) {
        var attr = this.attr(e);
        if (typeof attr !== typeof undefined && attr !== false)
            return true;
        return false;
    };
})(jQuery);
/* function Reset And Readonly Field*/
function setResetReadonly(className, set) {
    $('#bmr').val('');
    if ($('#bmr').attr('data-value') !== undefined) {
        $('#bmr').val($('#bmr').attr('data-value'));
    }
    $(className).each(function () {
        $(this).attr('readonly', false);
        $(this).val('');
    });
    if (set == '1') {
        $(className).each(function () {
            $(this).attr('readonly', true);
            $(this).val('');
        });
    }
    $(className).each(function () {
        var data_val = $(this).attr('data-value');
        if (data_val !== undefined) {
            $(this).val(data_val);
        }
    });
}
/* function Reset And Readonly Field*/
function showLoadingImage(className, set) {
    if (set == '1') {
        $(className).html('<div style="text-align:center;height:150px;line-height: 150px;"><img src="assets/global/img/loading-spinner-blue.gif"/>');
    }
    else {
        $(className).html('');
    }
}
/* Calculate BMR */
function calculateBMR(method, _return) {
    //=(IF(OR(D5="Female",D5="female"),9.99*D8+6.25*D7-4.92*D6-161,9.99*D8+6.25*D7-4.92*D6+5))
    var w_type = $('#weight_type').val();
    var age = $('.age_calculate').html();
    var height = $('#height').val();
    var weight = $('#weight').val();
    var fat = $('#fat').val();
    if (!method) {
        method = $('#method').val();
    }
    if (!method) {
        method = '19';
        $('#method option[value="19"]').attr("selected", true);
    }
    if (w_type === 'lbs') {
        /* Change in lbs */
        weight = weight / 2.20462;
    }
    var sex = $('.sex_name').html();

    var bmr = '';
    if (method == '15') {
//        var lean_body_mass = $('#lean_body_mass').val();
        var lean_body_mass = weight - (fat / 100) * weight;
        bmr = 370 + (21.6 * lean_body_mass);
    } else if (method == '16') {
        if (sex == "female") {
            bmr = 9.99 * weight + 6.25 * height - 4.92 * age - 161;
        } else {
            bmr = 9.99 * weight + 6.25 * height - 4.92 * age + 5;
        }
    } else if (method == '19') {
        if (sex == "female") {
            bmr = 655.0955 + (9.5634 * weight) + (1.8496 * height) - (4.6756 * age);
        } else {
            bmr = 66.473 + (13.7516 * weight) + (5.0033 * height) - (6.755 * age);
        }
    }
    if (_return === true) {
        return bmr ? Math.round(bmr) : '';
    }
    $('#bmr').val(bmr ? Math.round(bmr) : '');
}

(function ($) {
    $(document).ready(function () {
        $('[data-value]').each(function () {
            var data_val = $(this).attr('data-value');
            if (data_val !== undefined) {
                $(this).val(data_val).change();
            }
        });
        if ($('#calorie_profile_percent[data-value2]')) {
            setTimeout(function () {
                var data_val = $('#calorie_profile_percent').attr('data-value2');
                if (data_val !== undefined) {
                    $('#calorie_profile_percent').val(data_val).change();
                }
            }, 200);
        }
        if ($('#height[data-value]').val()) {
            setTimeout(function () {
                $('#weight').change();
                $('#height').change();
            }, 400);
        }
        if ($('#method').val()) {
            $('#method').change();
        }
        $('[data-toggle="chosen"]').chosen();
        $('#calorie_profile').change();
        $('.plan_user').on('change', function () {
            var user = $(this).val();
            if (user != '') {
                setResetReadonly('.userPlanInfo', '0');
                showLoadingImage('.show_plan_user_info', '1');
                var dataString = 'act_type=plan&sec_type=plan_user&user_id=' + user;
                $.ajax({
                    type: "POST",
                    url: "script/ajax.php",
                    data: dataString,
                    success: function (data) {
                        showLoadingImage('.show_plan_user_info', '0');
                        $('.show_plan_user_info').html(data);
                    },
                });
            }
            else {
                setResetReadonly('.userPlanInfo', '1');
            }
        });
        //print for report page
        $('#t_report_print').click(function () {
            $('#training_day').print({
                globalStyles: true,
                noPrintSelector: ".no-print",
                prepend: '<div class="row"><div class="col-md-12"><h1>Training Day Plan</h1></div></div><hr />'
            });
        });
        $('#nt_report_print').click(function () {
            $('#non_training_day').print({
                globalStyles: true,
                noPrintSelector: ".no-print",
                prepend: '<div class="row"><div class="col-md-12"><h1>Non-Training Day Plan</h1></div></div><hr />'
            });
        });
        //print for report page ends
        special_change();
        setTimeout(function () {
            special_change('#carbs');
//            console.clear();
        }, 1000);
        if ($('.plan_user').val()) {
            $('.plan_user').change().prop('disabled', true);
        }
        $('select#food_select').each(function () {
            if ($(this).val()) {
                $(this).change();
            }
        });
        if ($('.remove_food').length) {
            $('.foods_container').each(function () {
                var foods_container = $(this);
                foods_container.find('.add_food:not(:last)').hide();
            });
        }
        $('.food').each(function () {
            var el = $(this);
            if (el.val()) {
                el.change();
                $('#save_meal').removeClass('btn-primary').addClass('btn-success').html('<i class="fa fa-check"></i> Update Meal Plan');
            }
        });
        setTimeout(function () {
            $('.foods_panel > .panel-body').each(function () {
                if ($(this).find('[for="food"]').length > 2) {
                    var food_div = $(this);
                    food_div.find('.add_food:not(:last)').hide(400);
                }
            });
        }, 200);
    }).on('click', '.go2top', function (e) {
        e.preventDefault();
        $('body').scrollTop(1);
    }).on('change', '.userPlanInfo,.userPlanWeightType,#method', function () {
        calculateBMR();
    }).on('change', '#carbs', function () {
        $('#fats').val(100 - $(this).val());
        var methods = [15, 16, 19];
        var protein_calories = $('#protein_calories').val();
        var carbs = $('#carbs').val();
        for (var i in methods) {
            var method_id = methods[i];
            var total = $('#recommended_daily_cals [data-method_id="' + method_id + '"] #_total').text();
            if (total != '0') {
                var rec_carb = Math.round((total - protein_calories) * (carbs / 100));
                $('#recommended_daily_cals [data-method_id="' + method_id + '"] #_carb').text(rec_carb);
            }
        }
    }).on('change', '#fats', function () {
        $('#carbs').val(100 - $(this).val());
        var methods = [15, 16, 19];
        var protein_calories = $('#protein_calories').val();
        var fats = $('#fats').val();
        for (var i in methods) {
            var method_id = methods[i];
            var total = $('#recommended_daily_cals [data-method_id="' + method_id + '"] #_total').text();
            if (total != '0') {
                var rec_fat = Math.round((total - protein_calories) * (fats / 100));
                $('#recommended_daily_cals [data-method_id="' + method_id + '"] #_fat').text(rec_fat);
            }
        }
    }).on('change', '#nt_carbs', function () {
        $('#nt_fats').val(100 - $(this).val());
    }).on('change', '#nt_fats', function () {
        $('#nt_carbs').val(100 - $(this).val());
    }).on('change', '#total_meals', function () {
        var el = $(this);
        var tot_meals = Number(el.val());
//        alert('tot_meals: ' + tot_meals);
    }).on('change', '[data-trigger="save_plan_field"]', function () {
        var field = $(this).attr('data-field');
        var value = $(this).val();
        var plan_id = $('#plan_id').val();
        if (value) {
            $.post('script/ajax.php', {
                act_type: 'plan',
                sec_type: 'save_plan_field',
                plan_id: plan_id,
                field: field,
                value: value
            }, function () {
                if (field === 'carbs') {
                    $.post('script/ajax.php', {
                        act_type: 'plan',
                        sec_type: 'save_plan_field',
                        plan_id: plan_id,
                        field: 'fats',
                        value: $('#fats').val()
                    });
                }
                if (field === 'fats') {
                    $.post('script/ajax.php', {
                        act_type: 'plan',
                        sec_type: 'save_plan_field',
                        plan_id: plan_id,
                        field: 'carbs',
                        value: $('#carbs').val()
                    });
                }
            });
        }
    }).on('keyup', '[data-trigger="save_plan_field"]', function () {
        $(this).change();
    }).on('change', '.food', function () {
        var el = $(this);
        if (!el.val()) {
            return;
        }
        var k = el.attr('data-k');
        for (var i in foods) {
            var food = foods[i];
            if (food.id === el.val()) {
                break;
            }
        }
        $('#save_meal_tr').show();
//        handle_food(k, food);
    }).on('change', '.food_tr #amount', function () {
        $('.food[data-k="' + $(this).attr('data-k') + '"]').change();
    }).on('click', '#save_meal', function (e) {
        e.preventDefault();
        var btn = $(this);
        var plan_info_id = $(this).attr('data-plan_info_id');
        btn.prop('disabled', true);
        $.post('script/ajax.php', {
            act_type: 'plan',
            sec_type: 'del_meal_plan',
            plan_info_id: plan_info_id
        }, function () {
            btn.prop('disabled', false);
            $('.food_tr').each(function () {
                var tr = $(this);
                var food_id = tr.find('.food').val();
                var amount = Number(tr.find('#amount').val());
                if (food_id && amount > 0) {
                    var at_time = tr.find('#at_time').val();
                    btn.prop('disabled', true);
                    $.post('script/ajax.php', {
                        act_type: 'plan',
                        sec_type: 'save_meal_plan',
                        plan_info_id: plan_info_id,
                        at_time: at_time,
                        food_id: food_id,
                        amount: amount
                    }, function () {
                        btn.prop('disabled', false);
                        toastr.remove();
                        toastr.success("Operation performed Successfully.", "Success!");
                    });
                }
            });
        });
    }).on('shown.bs.collapse', '[data-id="collapse_div"]', function () {
        $('[data-target="#' + $(this).attr('id') + '"]').removeClass("btn-success btn-warning").addClass("btn-success");
        $('[data-target="#' + $(this).attr('id') + '"] i').removeClass("fa-edit fa-check-square-o").addClass("fa-check-square-o");
    }).on('hidden.bs.collapse', '[data-id="collapse_div"]', function () {
        $('[data-target="#' + $(this).attr('id') + '"]').removeClass("btn-success btn-warning").addClass("btn-warning");
        $('[data-target="#' + $(this).attr('id') + '"] i').removeClass("fa-edit fa-check-square-o").addClass("fa-edit");
    }).on('keyup', '#meal_name', function () {
        var el = $(this);
        var meal_div = $(el.parents('#meal_plan_div')[0]);
        meal_div.find('.meal_name').text(el.val());
    }).on('change', '#meal_category', function () {
        var el = $(this);
        var meal_div = $(el.parents('#meal_plan_div')[0]);
        meal_div.find('.meal_category').text(el.val());
    }).on('keyup', '#meal_proteins', function () {
        $('#meal_proteins').change();
    }).on('change', '#meal_proteins', function () {
        var el = $(this);
        var meal_div = $(el.parents('#meal_plan_div')[0]);
        meal_div.find('.meal_proteins').text(el.val());
        meal_div.find('#meal_calories').change();
    }).on('keyup', '#meal_calories', function () {
        $('#meal_calories').change();
    }).on('change', '#meal_calories', function () {
        var el = $(this);
        var meal_div = el.closest('#meal_plan_div');
        var meal_calories = Number(el.val());
        var meal_proteins = Number(meal_div.find('#meal_proteins').val());
        meal_div.find('.meal_calories').text(meal_calories);
        meal_div.find('#meal_carbs_fats').val(meal_calories - meal_proteins);
        meal_div.find('#meal_fats_carbs').change();
    }).on('keyup', '#meal_carbs', function () {
        $('#meal_carbs').change();
    }).on('change', '#meal_carbs', function () {
        var el = $(this);
        var meal_div = $(el.parents('#meal_plan_div')[0]);
        meal_div.find('.meal_carbs').text(el.val());
        handle_food(meal_div.find('.foods_container'));
    }).on('keyup', '#meal_fats', function () {
        $('#meal_fats').change();
    }).on('change', '#meal_fats', function () {
        var el = $(this);
        var meal_div = $(el.parents('#meal_plan_div')[0]);
        var meal_carbs_fats = meal_div.find('#meal_carbs_fats').val();
        meal_div.find('.meal_fats').text(el.val());
        meal_div.find('#meal_carbs').val(meal_carbs_fats - Number(el.val())).change();
    }).on('change', '#meal_fats_carbs', function () {
        var el = $(this);
        var meal_div = $(el.parents('#meal_plan_div')[0]);
        var meal_carbs_fats = meal_div.find('#meal_carbs_fats').val();
        meal_div.find('.meal_fats_carbs').text(String(el.val()) + '/' + String(100 - Number(el.val())));
        meal_div.find('#meal_fats').val(Math.round((Number(el.val()) / 100) * meal_carbs_fats)).change();
    }).on('change', '#meal_time', function () {
        var el = $(this);
        var meal_div = $(el.parents('#meal_plan_div')[0]);
        meal_div.find('.meal_time').text(el.val());
    }).on('click', '#meal_plan_div .add_food', function (e) {
        e.preventDefault();
        var plan_info_id = $('#plan_info_id').val();
        var el = $(this);
        var meal_div = el.closest('#meal_plan_div');
        var meal_num = meal_div.attr('data-meal');
        var foods_container = meal_div.find('.foods_container');
        var food_html = sync_call('script/ajax.php', {
            act_type: 'plan',
            sec_type: 'food_html',
            meal_num: meal_num,
        }, true, true);
        foods_container.find('.add_food').hide();
        foods_container.append(food_html);
        $('[data-toggle="chosen"]').chosen();
    }).on('click', '#meal_plan_div .remove_food', function (e) {
        e.preventDefault();
        var foods_container = $(this).closest('.foods_container');
        $(this).closest('[data-for="food"]').hide(0, function () {
            $(this).remove();
            foods_container.find('.add_food:last').show();
            handle_food(foods_container);
        });
    }).on('keyup', '#food_quantity', function () {
        $(this).change();
    }).on('change', '#food_quantity', function () {
        var foods_container = $(this).closest('.foods_container');
        handle_food(foods_container);
    }).on('change', '#food_select', function () {
        var foods_container = $(this).closest('.foods_container');
        var qty = $(this).closest('[data-for="food"]').find('#food_quantity').val();
        if (!qty) {
            $(this).closest('[data-for="food"]').find('#food_quantity').val(100);
        }
        handle_food(foods_container);
    }).on('change', '#weight,#weight_type,#fat,#calorie_profile_percent,#activity_factor', function () {
        $('#ppkg').change();
        var fat = $('#fat').val();
        var weight = $('#weight').val();
        var w_type = $('#weight_type').val();
        if (w_type === 'lbs') {
            /* Change in lbs */
            weight = Math.round(weight / 2.20462);
        }
        var lean_body_mass = weight - (fat / 100) * weight;
        $('#lean_body_mass').val(lean_body_mass);
        var activity_factor_id = $('#activity_factor').val();
        var activity_factor = getActivityFactor(activity_factor_id, 'description');
        var percent = $('#calorie_profile_percent').val();
        //----------------
        var method_id = '15';
        var calories = calculateBMR(method_id, true);
        if (!calories)
            calories = 0;
        $('#bmr_info_body [data-method_id="' + method_id + '"] #_calories').text(calories);
        var _total = calories * activity_factor;
        $('#estimated_daily_cals [data-method_id="' + method_id + '"] #_total').text(Math.round(_total));
        $('#fats,#carbs').change();
        var target_calories = Math.round(_total * (1 + (percent / 100)));
        //recommended_total
        $('#recommended_daily_cals [data-method_id="' + method_id + '"] #_total').text(target_calories);
        //---------------------------------
        if ($('#method').val() == method_id) {
            $('#target_calories').val(target_calories);
        }
        //---------------------------------
        $('#estimated_daily_cals [data-method_id="' + method_id + '"] #_protein').text(Math.round(_total * 0.15));
        $('#estimated_daily_cals [data-method_id="' + method_id + '"] #_fat').text(Math.round(_total * 0.35));
        $('#estimated_daily_cals [data-method_id="' + method_id + '"] #_carb').text(Math.round(_total * 0.5));
        //----------------
        var calories = calculateBMR(15, true);
        if (!calories)
            calories = 0;
        $('#bmr_info_body [data-method_id="15"] #_calories').text(calories);
    }).on('keyup', '#ppkg', function () {
        $('#ppkg').change();
    }).on('change', '#protein_calories', function () {
        $('#fats,#carbs').change();
    }).on('change', '#ppkg', function () {
        var weight = $('#weight').val();
        var w_type = $('#weight_type').val();
        if (w_type === 'lbs') {
            /* Change in lbs */
            weight = Math.round(weight / 2.20462);
        }
        var protein_per_day = Number(weight) * Number($(this).val());
        $('#protein_per_day').val(protein_per_day);
        var protein_calories = protein_per_day * 4;
        $('#protein_calories').val(protein_calories).change();
        $('[data-for="recommended_daily_cal"]').each(function () {
            $(this).find('#_protein').text(protein_calories);
        });
    }).on('change', '#calorie_profile', function () {
        var el = $(this);
        var calorie_profile = el.val();
        $('#calorie_profile_percent').val('');
        if (calorie_profile) {
            $('#calorie_profile_percent').val(calorie_profiles[calorie_profile].default).change();
        }
    }).on('change', '#weight,#height,#user_id,#activity_factor,#calorie_profile_percent', function () {
        var activity_factor_id = $('#activity_factor').val();
        var activity_factor = getActivityFactor(activity_factor_id, 'description');
        var percent = $('#calorie_profile_percent').val();
        methods = [16, 19];
        for (var i in methods) {
            var method_id = methods[i];
            //-------------------------
            var calories = calculateBMR(method_id, true);
            if (!calories)
                calories = 0;
            $('#bmr_info_body [data-method_id="' + method_id + '"] #_calories').text(calories);
            var _total = calories * activity_factor;
            $('#estimated_daily_cals [data-method_id="' + method_id + '"] #_total').text(Math.round(_total));
            $('#fats,#carbs').change();
            var target_calories = Math.round(_total * (1 + (percent / 100)));
            //recommended_total
            $('#recommended_daily_cals [data-method_id="' + method_id + '"] #_total').text(target_calories);
            //---------------------------------
            if ($('#method').val() == method_id) {
                $('#target_calories').val(target_calories);
            }
            //---------------------------------
            $('#estimated_daily_cals [data-method_id="' + method_id + '"] #_protein').text(Math.round(_total * 0.15));
            $('#estimated_daily_cals [data-method_id="' + method_id + '"] #_fat').text(Math.round(_total * 0.35));
            $('#estimated_daily_cals [data-method_id="' + method_id + '"] #_carb').text(Math.round(_total * 0.5));

            //-------------------------
            var calories = calculateBMR(method_id, true);
            if (!calories)
                calories = 0;
            $('#bmr_info_body [data-method_id="' + method_id + '"] #_calories').text(calories);
            var _total = Math.round(calories * activity_factor);
            $('#estimated_daily_cals [data-method_id="' + method_id + '"] #_total').text(_total);
            $('#estimated_daily_cals [data-method_id="' + method_id + '"] #_protein').text(Math.round(_total * 0.15));
            $('#estimated_daily_cals [data-method_id="' + method_id + '"] #_fat').text(Math.round(_total * 0.35));
            $('#estimated_daily_cals [data-method_id="' + method_id + '"] #_carb').text(Math.round(_total * 0.5));
        }
        //for 16
        //for 19
        var calories = calculateBMR(19, true);
        if (!calories)
            calories = 0;
        $('#bmr_info_body [data-method_id="19"] #_calories').text(calories);
    }).on('change', '#height,#weight,#weight_type,#fat', function () {
        var height = $('#height').val();
        if (!height || height == '0') {
            $('#bmi,#lean_bmi').val('0.0');
            return;
        }
        var weight = $('#weight').val();
        var weight_type = $('#weight_type').val();
        if (weight_type === 'lbs')
            weight = weight / 2.20462;
        var fat = $('#fat').val();
        var lean_body_mass = weight - (fat / 100) * weight;
        if (weight && height) {
            $('#bmi').val(((weight * 10000) / Math.pow(height, 2)).toFixed(1));
            $('#lean_bmi').val(((lean_body_mass * 10000) / Math.pow(height, 2)).toFixed(1));
        }
    });
})(jQuery);

function special_change(selector, attr) {
    if (!selector)
        selector = '#fats';
    if (!attr)
        attr = 'data-original_value';
    var el = $(selector);
    var val = el.val();
    if (!el.hasAttr(attr)) {
        el.attr(attr, val);
    }
    var attr_val = el.attr(attr);
    if (val !== attr_val) {
        el.attr(attr, val).change();
    }
    setTimeout(function () {
        special_change(selector, attr);
    }, 200);
}

function get_food(food_id) {
    for (var i in foods) {
        var food = foods[i];
        if (food.id === food_id) {
            break;
        }
    }
    return food;
}

function getActivityFactor(activity_factor_id, field) {
    for (var i in activity_factors) {
        var activity_factor = activity_factors[i];
        if (activity_factor.id === activity_factor_id) {
            break;
        }
    }
    return field ? activity_factor[field] : activity_factor;
}

function handle_food(foods_container) {
    if (!foods_container) {
        $('.foods_container').each(function () {
            handle_food($(this));
        });
        return;
    }
//    var food_type = foods_panel.attr('data-food_type');
    var meal_plan_div = foods_container.closest('#meal_plan_div');
    var meal_proteins = meal_plan_div.find('#meal_proteins').val();
    var meal_fats = meal_plan_div.find('#meal_fats').val();
    var meal_carbs = meal_plan_div.find('#meal_carbs').val();
//    var foods = 0;
//    foods_container.find('[data-for="food"]').each(function () {
//        var food_row = $(this);
//        var select = food_row.find('#food_select');
//        var food_id = select.val();
//        if (food_id) {
//            foods++;
//        }
//    });
//    var per_food_protein = meal_proteins / foods;
//    var per_food_fat = meal_fats / foods;
//    var per_food_carb = meal_carbs / foods;
    var meal_calculated_protein = 0;
    var meal_calculated_carb = 0;
    var meal_calculated_fat = 0;
    foods_container.find('[data-for="food"]').each(function () {
        var food_row = $(this);
        var select = food_row.find('#food_select');
        var input = food_row.find('#food_quantity');
        var quantity = input.val();
        var food_id = select.val();
        if (food_id && quantity) {
            var food = get_food(food_id);
            var food_unit = food.unit;
            var food_protein = Math.round((food.protein * 4) * (quantity / 100));
            var food_carb = Math.round((food.carb * 4) * (quantity / 100));
            var food_fat = Math.round((food.fat * 9) * (quantity / 100));
            meal_calculated_protein += food_protein;
            meal_calculated_carb += food_carb;
            meal_calculated_fat += food_fat;
            food_row.find('[data-food_type="protein"]').text(food_protein);
            food_row.find('[data-food_type="carb"]').text(food_carb);
            food_row.find('[data-food_type="fat"]').text(food_fat);
            food_row.find('[data-id="unit"]').text(food_unit);
//            if (food_type == 'protein') {
//                var input_qty = per_food_protein / food_protein;
//            }
//            if (food_type == 'fat') {
//                var input_qty = per_food_fat / food_fat;
//            }
//            if (food_type == 'carb') {
//                var input_qty = per_food_carb / food_carb;
//            }
//            input.val(input_qty.toFixed(2));
//        } else {
//            input.val('');
        }
    });
    //remove errors by default
    foods_container.next().find('#meal_calculated_protein').parent().removeClass('danger');
    foods_container.next().find('#meal_calculated_carb').parent().removeClass('danger');
    foods_container.next().find('#meal_calculated_fat').parent().removeClass('danger');
    //set values
    foods_container.next().find('#meal_calculated_protein').text(meal_calculated_protein);
    foods_container.next().find('#meal_calculated_carb').text(meal_calculated_carb);
    foods_container.next().find('#meal_calculated_fat').text(meal_calculated_fat);
    //set error if calculated value is more
    if (meal_calculated_protein > meal_proteins) {
        foods_container.next().find('#meal_calculated_protein').parent().addClass('danger');
    }
    if (meal_calculated_carb > meal_carbs) {
        foods_container.next().find('#meal_calculated_carb').parent().addClass('danger');
    }
    if (meal_calculated_fat > meal_fats) {
        foods_container.next().find('#meal_calculated_fat').parent().addClass('danger');
    }
}

function calculate_food_totals() {
    var tot_tr = $('#tot_meals');
    var calories = 0;
    var carb = 0;
    var fat = 0;
    var protein = 0;
    $('.food_tr').each(function () {
        var tr = $(this);
        calories += Number(tr.find('#calories').text());
        carb += Number(tr.find('#carb').text());
        fat += Number(tr.find('#fat').text());
        protein += Number(tr.find('#protein').text());
    });
    tot_tr.find('#calories').text(calories);
    tot_tr.find('#carb').text(carb);
    tot_tr.find('#fat').text(fat);
    tot_tr.find('#protein').text(protein);
    var target_calories = Number($('#target_calories').val());
    var target_proteins = Number($('#target_proteins').val());
    tot_tr.removeClass("warning danger success");
    if (calories > target_calories || protein > target_proteins) {
        tot_tr.addClass("danger");
    } else if (calories === target_calories && protein === target_proteins) {
        tot_tr.addClass("success");
    } else {
        tot_tr.addClass("warning");
    }
}

function sync_call(url, data, post_call, original_response) {
    post_call = typeof post_call === 'boolean' ? post_call : false;
    original_response = typeof original_response !== 'undefined' ? original_response : false;
    $.ajaxSetup({
        async: false
    });
    if (post_call === true) {
        if (original_response === true)
            return $.post(url, data).responseText;
        return $.parseJSON($.post(url, data).responseText);
    }
    if (original_response === true)
        return $.get(url, data).responseText;
    return $.parseJSON($.get(url, data).responseText);
}
