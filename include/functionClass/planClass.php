<?php

class plan extends cwebc {

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('plan');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'user_id', 'created_by', 'is_active', 'is_deleted', 'date_add');
    }

    function savePlan($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getPlan($id) {
        return $this->_getObject($this->TableName, $id);
    }

    function listPlan($skip_active = FALSE) {
        $this->Where = "where is_deleted='0'";
        if ($skip_active !== TRUE) {
            $this->Where .= " AND is_active='1'";
        }
        $this->Where .= " order by $this->orderby $this->order";
        return $this->ListOfAllRecords('object');
    }

    function listTrashPlan() {
        $this->Where = "where is_deleted='1' order by $this->orderby $this->order";
        return $this->ListOfAllRecords('object');
    }

    function deletePlan($id) {
        $this->id = $id;
        if (SOFT_DELETE)
            return $this->SoftDelete();
        return $this->Delete();
    }

    public static function valid($id) {
        $obj = new plan;
        $obj->Field = "COUNT(*) as ct";
        $obj->Where = "WHERE id=$id";
        $data = $obj->DisplayOne();
        return $data->ct ? true : false;
    }

}

class plan_info extends cwebc {

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('plan_info');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'user_id', 'plan_id', 'plan_type', 'weight_type', 'weight', 'height', 'fat', 'bmr', 'age', 'activity_factor', 'training_per_week', 'training_time', 'method', 'total_meals', 'cardio_per_week', 'wake_up_time', 'sleep_time', 'carbs', 'fats', 'protein_per_day', 'target_calories', 'day_calories', 'date_add', 'ppkg', 'calorie_profile', 'calorie_profile_percent', 'differential');
    }

    function savePlanInfo($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
//        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getPlanInfo($plan_id) {
        $this->Where = "WHERE `plan_id`='$plan_id'";
//        $this->print = 1;
        return $this->ListOfAllRecords('object');
//        return $this->_getObject($this->TableName, $id);
    }

    function deletePlanInfo($id) {
        $this->id = $id;
        if (SOFT_DELETE)
            return $this->SoftDelete();
        return $this->Delete();
    }

}

class plan_meals extends cwebc {

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('plan_meals');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'plan_id', 'plan_info_id', 'meal_name', 'meal_category', 'meal_proteins', 'meal_carbs', 'meal_fats', 'meal_calories', 'meal_time', 'meal_comments', 'is_active', 'date_add');
    }

    static function hasBothPlans($plan_id) {
        $obj = new plan_info;
        $obj->Field = "id";
        $obj->Where = "WHERE plan_id=$plan_id";
        $data = $obj->ListOfAllRecords('');
        $ids = '';
        foreach ($data as $temp) {
            $ids .= $temp->id . ',';
        }
        $ids = rtrim($ids, ',');
        $obj = new plan_meals;
        $obj->Field = "id";
        $obj->Where = "WHERE plan_info_id IN ($ids) GROUP BY plan_info_id";
        $data = $obj->ListOfAllRecords('');
        return count($data) == 2 ? TRUE : FALSE;
    }

    function savePlanMeal($data) {
        $this->Data = $this->_makeData($data, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

}

class meal_foods extends cwebc {

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('meal_foods');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'meal_id', 'food_type', 'food_id', 'quantity', 'is_active', 'date_add');
    }

    function saveMealFood($data) {
        $this->Data = $this->_makeData($data, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    public static function getMealFoods($meal_id, $food_type = '') {
        $obj = new meal_foods;
        $obj->Where = "WHERE meal_id=$meal_id";
        if ($food_type) {
            $obj->Where .= " AND food_type='$food_type'";
        }
        $data = $obj->ListOfAllRecords('');
        return !empty($data) ? $data : FALSE;
    }

}

?>