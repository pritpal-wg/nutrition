<?php

/*
 * food Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class food extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'name') {
        parent::__construct('food');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'unit', 'calories', 'food_type', 'protein', 'fat', 'carb', 'description', 'is_active', 'is_deleted', 'add_date', 'upd_date');
    }

    /*
     * Create new or update existing 
     */

    function saveFood($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Data['add_date'] = date('Y-m-d H:i:s');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get Food by id
     */

    function getFood($id) {
        return $this->_getObject('food', $id);
    }

    /*
     * Get List of all Food in object array
     */

    function listFood() {
        $this->Where = "where is_deleted='0' order by $this->orderby $this->order";
        return $this->ListOfAllRecords('object');
    }

    function listTrashFood() {
        $this->Where = "where is_deleted='1' order by $this->orderby $this->order";
        return $this->ListOfAllRecords('object');
    }

    /*
     * delete a Food by id
     */

    function deleteFood($id) {
        $this->id = $id;
        if (SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }

}

#Food Category

class foodCat extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'name') {
        parent::__construct('food_cat');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'parent_id', 'description', 'is_active', 'is_deleted', 'add_date', 'upd_date');
    }

    /*
     * Create new or update existing 
     */

    function saveFoodCat($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Data['add_date'] = date('Y-m-d H:i:s');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get Food Cat by id
     */

    function getFoodCat($id) {
        return $this->_getObject('food_cat', $id);
    }

    /*
     * Get List of all Food Cat in object array
     */

    function listFoodCat($active = '0') {
        if ($active == '1'):
            $this->Where = "where is_deleted='0' AND is_active='1' order by $this->orderby $this->order";
        else:
            $this->Where = "where is_deleted='0' order by $this->orderby $this->order";
        endif;
        return $this->ListOfAllRecords('object');
    }

    function listTrashFoodCat() {
        $this->Where = "where is_deleted='1' order by $this->orderby $this->order";
        return $this->ListOfAllRecords('object');
    }

    /*
     * delete a Food Cat by id
     */

    function deleteFoodCat($id) {
        $this->id = $id;
        return $this->Delete();
    }

    function getTopCats($id = null) {
        $this->Field = "id,name";
        $this->Where = "WHERE parent_id=0 AND is_active=1";
        if (is_numeric($id) && $id > 0) {
            $this->Where .= " AND id !=$id";
        }
        $this->Where .= " ORDER BY name";
        return $this->ListOfAllRecords('');
    }

    function getCategories($parent_id = 0) {
        $this->Field = "id, name";
        $this->Where = "WHERE parent_id=$parent_id AND is_active=1 ORDER BY name";
        return $this->ListOfAllRecords('');
    }

}

#Food Cat Rel

class foodCatRel extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'name') {
        parent::__construct('food_cat_rel');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'food_id', 'cat_id', 'add_date');
    }

    function saveRecord($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Data['add_date'] = date('Y-m-d H:i:s');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function saveFoodCatRel($POST, $food_id) {
        foreach ($POST as $key => $val):
            $rec = array('cat_id' => $val, 'food_id' => $food_id);
            $QueryObj = new foodCatRel;
            $QueryObj->saveRecord($rec);
        endforeach;
        return false;
    }

    /*
     * Get Food Cat Rel by id
     */

    function getFoodCat($id) {
        return $this->_getObject('food_cat', $id);
    }

    /*
     * Get List of all Food Cat in object array
     */

    function getFoodCatRelInArray($food) {
        $res = array();
        $this->Where = "where food_id='" . $food . "'";
        $rec = $this->ListOfAllRecords('object');
        foreach ($rec as $key => $val):
            $res[] = $val->cat_id;
        endforeach;
        return $res;
    }

    function deleteOldRel($food) {
        $this->Where = "where food_id='" . $food . "'";
        return $this->Delete_where();
    }

}

?>