<?php
/*
 * food Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */
class user extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */
    function __construct($order='asc', $orderby='first_name'){
        parent::__construct('user');
		$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id', 'first_name', 'last_name','email', 'gender', 'address', 'contact', 'date_of_birth', 'is_active', 'is_deleted', 'add_date', 'upd_date');
    }

    /*
     * Create new or update existing 
     */
    function saveUser($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';  
		if(!empty($POST['date_of_birth'])):
			$this->Data['date_of_birth']=date('Y-m-d',strtotime($POST['date_of_birth']));
		endif;
        if(isset($this->Data['id']) && $this->Data['id']!=''){
            if($this->Update())
              return $Data['id'];
        }
        else{
			$this->Data['add_date']=date('Y-m-d H:i:s');
            $this->Insert();
            return $this->GetMaxId();
        }
    }
        
    /*
     * Get User by id
     */
    function getUser($id){
        return $this->_getObject('user', $id);
    }
    
    
    /*
     * Get List of all User in object array
     */
    function listUsers(){
        $this->Where="where is_deleted='0' order by $this->orderby $this->order";
        return $this->ListOfAllRecords('object');    
    }
	
    function limitedUserInfo(){
        $this->Field=('id,first_name,last_name');
		$this->Where="where is_deleted='0' AND is_active='1' order by $this->orderby $this->order";
        return $this->ListOfAllRecords('object');    
    }	
	
	
    function checkField($field,$value){
        $this->Where="where `".$field."`= '".$value."'";
        return $this->DisplayOne();    
    }	
	
	
    function listTrashUser(){
        $this->Where="where is_deleted='1' order by $this->orderby $this->order";
        return $this->ListOfAllRecords('object');    
    }	
	
    
    /*
     * delete a User by id
     */
    function deleteUser($id){
        $this->id=$id;
        if(SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }

}
?>