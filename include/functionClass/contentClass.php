<?php
/*
 * Content Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class content extends cwebc {
    
    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;
    
    /*
     * 
     */
    function __construct($order='asc', $orderby='position'){
        $this->InitilizeSQL();
        $this->orderby=$orderby;
        $this->parent_id=$parent_id;
        $this->order=$order;
        parent::__construct('content');
        //$this->TableName='content';
        $this->requiredVars=array('id', 'name', 'urlname', 'page', 'page_name', 'meta_keyword', 'meta_description', 'position', 'mobile_site', 'mobile_site_content', 'is_deleted', 'publish_date', 'parent_id','image');
    }

    /*
     * Create new page or update existing page
     */
    function savePage($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        
        if($this->Data['name']==''){
            $this->Data['name']=$this->Data['page_name'];
        }
        
        if($this->Data['meta_keyword']==''){
            $this->Data['meta_keyword']=$this->Data['page_name'];
        }
        
        if($this->Data['meta_description']==''){
            $this->Data['meta_description']=$this->Data['page_name'];
        }
        
        if($this->Data['urlname']==''){
            $this->Data['urlname']=$this->_sanitize($this->Data['page_name']);
        }
        
        if(isset($this->Data['id']) && $this->Data['id']!=''){
            $rand=rand(0, 99999999);
            if(upload_photo('content', $_FILES['image'], $rand))
                    $this->Data['image']=make_file_name($_FILES['image']['name'], $rand);
            if($this->Update())
              return $Data['id'];
        }
        else{
            $rand=rand(0, 99999999);
            if(upload_photo('content', $_FILES['image'], $rand))
                    $this->Data['image']=make_file_name($_FILES['image']['name'], $rand);
            $this->Insert();
            return $this->GetMaxId();
        }
    }
        
    /*
     * Get page by id
     */
    function getPage($id){
        return $this->_getObject('content', $id);
    }
    
    
    /*
     * Get List of all pages in array
     */
    function listPages(){
        $this->Where="where is_deleted='0' order by $this->orderby $this->order";
        $this->DisplayAll();        
    }
    
    /*
     * delete a page by id
     */
    function deletePage($id){
        $this->id=$id;
        if(SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }
    
    /*
     * Update page position
     */
    
    function updatePosition($position, $id){
        $this->Data['id']=$id;
        $this->Data['position']=($position!='')?$position:0;
        $this->Update();
    }
    
    function getPageContent($id){
        return html_entity_decode($this->getPage($id)->page);
    }
    
    function getPageTitle($id){
        return $this->getPage($id)->page_name;
    }
    
    function getPageSlug($id){
        return $this->getPage($id)->urlname;
    }
}
?>