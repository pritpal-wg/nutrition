<?php
/*
 * food Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

#Activity Factor Category

class activityFactor extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */
    function __construct($order='asc', $orderby='name'){
        parent::__construct('activity_factor');
		$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id', 'name', 'description',  'is_active', 'is_deleted', 'add_date', 'upd_date');
    }

    /*
     * Create new or update existing 
     */
    function saveRecord($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';  
		
        if(isset($this->Data['id']) && $this->Data['id']!=''){
            if($this->Update())
              return $Data['id'];
        }
        else{
			$this->Data['add_date']=date('Y-m-d H:i:s');
            $this->Insert();
            return $this->GetMaxId();
        }
    }
        
    /*
     * Get Food Cat by id
     */
    function getRecord($id){
        return $this->_getObject('activity_factor', $id);
    }
    
    
    /*
     * Get List of all in object array
     */
    function listRecords($active='0'){
		if($active=='1'):
			$this->Where="where is_deleted='0' AND is_active='1' order by $this->orderby $this->order";
		else:
			$this->Where="where is_deleted='0' order by $this->orderby $this->order";
		endif;	
        return $this->ListOfAllRecords('object');    
    }
	
    function listTrashed(){
        $this->Where="where is_deleted='1'";
        return $this->ListOfAllRecords('object');    
    }
	
    
    /*
     * delete by id
     */
    function deleteRecord($id,$force = FALSE){
        $this->id=$id;
        return $force === TRUE ? $this->Delete() : $this->SoftDelete();
    }

    function restoreRecord($id){
        $this->id=$id;
        $this->Restore();
    }

}


#Method List

class methodList extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */
    function __construct($order='asc', $orderby='name'){
        parent::__construct('method');
		$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id', 'name', 'description',  'is_active', 'is_deleted', 'add_date', 'upd_date');
    }

    /*
     * Create new or update existing 
     */
    function saveRecord($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';  
		
        if(isset($this->Data['id']) && $this->Data['id']!=''){
            if($this->Update())
              return $Data['id'];
        }
        else{
			$this->Data['add_date']=date('Y-m-d H:i:s');
            $this->Insert();
            return $this->GetMaxId();
        }
    }
        
    /*
     * Get rec by id
     */
    function getRecord($id){
        return $this->_getObject('method', $id);
    }
    
    
    /*
     * Get List of all in object array
     */
    function listRecords($active='0'){
		if($active=='1'):
			$this->Where="where is_deleted='0' AND is_active='1' order by $this->orderby $this->order";
		else:
			$this->Where="where is_deleted='0' order by $this->orderby $this->order";
		endif;	
        return $this->ListOfAllRecords('object');    
    }
	
	
    
    /*
     * delete by id
     */
    function deleteRecord($id){
        $this->id=$id;
        return $this->Delete();
    }

}



?>