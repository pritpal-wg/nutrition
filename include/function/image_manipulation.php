<?php
function resize_image($url, $width='', $height='')
{
	if(isset($url))
	{
		$type='';
		$image=$url;
		if(!empty($image) and file_exists($image)):
			$type=substr($image, -3);			
			if($type=='jpg'):
				$ImageObj = imagecreatefromjpeg($image);
			elseif($type=='gif'):
				$ImageObj = imagecreatefromgif($image);
			endif;
		else:
			$ImageObj = imagecreatefromjpeg("../graphic/no_image.jpg");
		endif;
	}
	
	$Height = isset($height) ? $width:100;
	$Width = isset($height) ? $width:100;

	$NewHeight = $Height;
	$NewWidth = $Width;

	if(imagesx($ImageObj) >= $Width)
	{
		$NewHeight = imagesy($ImageObj) / imagesx($ImageObj) * $Width;
		if($NewHeight > $Height)
		{
			$NewHeight = $Height;
			$NewWidth = imagesx($ImageObj) / imagesy($ImageObj) * $Height;
		}
	}
	elseif (imagesy($ImageObj) >= $Height)
	{
		$NewWidth = imagesx($ImageObj) / imagesy($ImageObj) * $Height;
		if($NewWidth > $Width)
		{
			$NewWidth = $Width;
			$NewHeight = imagesy($ImageObj) / imagesx($ImageObj) * $Width;
		}
	}
	else
	{
		$NewHeight = imagesy($ImageObj);
		$NewWidth = imagesx($ImageObj);
	}
	$ImageNewObj = imagecreatetruecolor($NewWidth,$NewHeight);
	imagecopyresampled($ImageNewObj,$ImageObj,0,0,0,0,$NewWidth,$NewHeight,imagesx($ImageObj),imagesy($ImageObj));
	if($type=='gif'):
		ob_start();
		header("content-type: image/gif");
		imagegif($ImageNewObj);
		
	elseif($type=='jpg'):
		ob_start();
		header("content_type: image/jpeg");
		imagejpeg($ImageNewObj);
		exit;
	endif;
	
}

function link_image($name)
{
	return '<img src="'.DIR_WS_SITE_GRAPHIC.$name.'" border="0">';
}

function create_thumb($type, $image){
        // Get the original geometry and calculate scales
	$source_path=DIR_FS_SITE.'upload/photo/'.$type.'/large/'.$image;
	$destination_path=DIR_FS_SITE.'upload/photo/'.$type.'/thumb/'.$image;
	
	list($width, $height) = getimagesize($source_path);
    $xscale=$width/THUMB_WIDTH;
    $yscale=$height/THUMB_HEIGHT;
    // Recalculate new size with default ratio
    if ($yscale>$xscale){   // portrait
		$new_width = round($width * (1/$yscale));
        $new_height = round($height * (1/$yscale));
    }
    else {					//landscape
        $new_width = round($width * (1/$xscale));
        $new_height = round($height * (1/$xscale));
    }

    // Resize the original image & output
    $imageResized = imagecreatetruecolor($new_width, $new_height);

    #check image format and create image
    if(strtolower(substr($source_path, -3))=='jpg'):
         $imageTmp     = imagecreatefromjpeg ($source_path);
    elseif(strtolower(substr($source_path, -3))=='gif'):
         $imageTmp     = imagecreatefromgif($source_path);
    elseif(strtolower(substr($source_path, -3))=='png'):
          $imageTmp     = imagecreatefrompng($source_path);
    endif;

    imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
    output_img($imageResized, image_type_to_mime_type(getimagesize($source_path)), $destination_path);
}

function create_resized_crop($type, $image, $folderName, $Rwidth, $Rheight){
        /* Get the original geometry and calculate scales */
	$source_path=DIR_FS_SITE.'upload/photo/'.$type.'/large/'.$image;
	$destination_path=DIR_FS_SITE.'upload/photo/'.$type.'/'.$folderName.'/'.$image;
	
        if(!is_dir(DIR_FS_SITE.'upload/photo/'.$type.'/'.$folderName)){
		@mkdir(DIR_FS_SITE.'upload/photo/'.$type.'/'.$folderName, 644);   //maximum 777
	}
        
	list($width, $height, $img_type, $attr) = getimagesize($source_path);
       
	/* should we resize the image? only if height and width both are higher than the base values */
	if($width>$Rwidth && $height>$Rheight){
                $xscale=$width/$Rwidth;
                $yscale=$height/$Rheight;
		
                /* Yes - we should resize it */
		/* Recalculate new size with default ratio */
		if ($yscale>$xscale){   
                    /* image is portrait */
                    $new_width = round($width * (1/$xscale));
                    $new_height = round($height * (1/$xscale));
		}
		else {					
                    /* image is landscape */
                    $new_width  = round($width  * (1/$yscale));
                    $new_height = round($height * (1/$yscale));
		}
		/* Resize the original image & output */
		$imageResized = imagecreatetruecolor($new_width, $new_height);
		/* check image format and create image */
		if(strtolower(substr($source_path, -3))=='jpg')
			 $imageTmp     = imagecreatefromjpeg ($source_path);
		elseif(strtolower(substr($source_path, -3))=='gif')
			 $imageTmp     = imagecreatefromgif($source_path);
		elseif(strtolower(substr($source_path, -3))=='png')
			  $imageTmp     = imagecreatefrompng($source_path);
                
		imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		output_img($imageResized, image_type_to_mime_type($img_type), $destination_path);
                crop_image($type, $image, $folderName, $Rwidth, $Rheight);
        }
	else{
                /* We should crop the image */
                /* firstly, we should copy the image to right folder */
                if(copy($source_path, $destination_path)){
                   crop_image($type, $image, $folderName, $Rwidth, $Rheight);
                }
                else{
                   echo 'sorry! could not copy the file to destination folder. Plese check folder permissions';
                }
        }
}

function crop_image($type, $image, $folder, $twidth, $theight){ 
	$source_path=DIR_FS_SITE.'upload/photo/'.$type.'/'.$folder.'/'.$image;
	$destination_path=DIR_FS_SITE.'upload/photo/'.$type.'/'.$folder.'/'.$image;
	list($width, $height) = getimagesize($source_path);
        
        $new_width=$twidth;
        $new_height=$theight;
        
        if($width<=$twidth){ $new_width=$width; }
	if($height<=$theight){ $new_height=$height; }	
        
	$cropping=cropFromCenter($twidth, $theight, $width, $height);
	if(count($cropping))
	{ 								
            $cropX=$cropping['cropX'];
            $cropY=$cropping['cropY'];									

            $imageResized = imagecreatetruecolor($new_width, $new_height);	

            #check image format and create image
            if(strtolower(substr($source_path, -3))=='jpg'):
                     $imageTmp     = imagecreatefromjpeg ($source_path);
            elseif(strtolower(substr($source_path, -3))=='gif'):
                     $imageTmp     = imagecreatefromgif($source_path);
            elseif(strtolower(substr($source_path, -3))=='png'):
                      $imageTmp     = imagecreatefrompng($source_path);
            endif;
			
            //imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
            imagecopyresampled($imageResized, $imageTmp, 0, 0, $cropX, $cropY, $new_width, $new_height, $new_width, $new_height);
            @output_img($imageResized, image_type_to_mime_type(getimagesize($source_path)), $destination_path);
	}	
}


/**
 * Crops the image from calculated center in a square of $cropSize pixels
 *
 * @param int $cropSize
 */

function cropFromCenter($new_w, $new_h, $curr_w, $curr_h) {

        if($new_w > $curr_w)$new_w=$curr_w;
        if($new_h > $curr_h)$new_h=$curr_h;

        $cropping=array();
    $cropX = intval(($curr_w - $new_w) / 2);
    $cropY = intval(($curr_h - $new_h) / 2);		

        $cropping['cropX']=$cropX;
        $cropping['cropY']=$cropY;

        return $cropping;
}

function create_resized_for_module($module, $large_image){
	global $conf_resizedCriteria;
	if(isset($conf_resizedCriteria[$module]) && is_array($conf_resizedCriteria[$module])){
		foreach($conf_resizedCriteria[$module] as $k=>$v){
			//create_resized($module, $large_image, $k, $v['width'], $v['height']);
                        create_resized_crop($module, $large_image, $k, $v['width'], $v['height']);
		}
	}
}

/*
 * Please use this function very carefully. It shall resize all the images for your project. 
 */
function resize_all_images(){
    set_time_limit(0);
    global $conf_resizedCriteria_resizing;
    foreach($conf_resizedCriteria_resizing as $kk=>$vv){
        if(isset($conf_resizedCriteria_resizing[$kk]) && is_array($conf_resizedCriteria_resizing[$kk])){
            foreach($conf_resizedCriteria_resizing[$kk] as $k=>$v){
                $source_dir=DIR_FS_SITE.'upload/photo/'.$kk.'/large/';
                $images_to_resize = scandir($source_dir);
                if(count($images_to_resize)){
                    foreach($images_to_resize as $kt=>$vt){
                        if(strtolower(substr($vt, -3))=='jpg' or strtolower(substr($vt, -3))=='png' or strtolower(substr($vt, -3))=='gif'){
                            create_resized_crop($kk, $vt, $k, $v['width'], $v['height']);
                        }
                    }
                }
            }
        }
    }
}


function create_resized($type, $image, $folderName, $Rwidth, $Rheight){
    
	// Get the original geometry and calculate scales
	$source_path=DIR_FS_SITE.'upload/photo/'.$type.'/large/'.$image;
	$destination_path=DIR_FS_SITE.'upload/photo/'.$type.'/'.$folderName.'/'.$image;
	
	if(!is_dir($DIR_FS_SITE.'upload/photo/'.$type.'/'.$folderName)){
		@mkdir($DIR_FS_SITE.'upload/photo/'.$type.'/'.$folderName, 644);   //maximum 777
	}
		
	list($width, $height) = getimagesize($source_path);
        $xscale=$width/$Rwidth;
        $yscale=$height/$Rheight;
    
        // Recalculate new size with default ratio
        if ($yscale>$xscale){
            $new_width = round($width * (1/$yscale));
            $new_height = round($height * (1/$yscale));
        }
        else {
            $new_width = round($width * (1/$xscale));
            $new_height = round($height * (1/$xscale));
        }

    // Resize the original image & output
    $imageResized = imagecreatetruecolor($new_width, $new_height);

    #check image format and create image
    if(strtolower(substr($source_path, -3))=='jpg'):
         $imageTmp     = imagecreatefromjpeg ($source_path);
    elseif(strtolower(substr($source_path, -3))=='gif'):
         $imageTmp     = imagecreatefromgif($source_path);
    elseif(strtolower(substr($source_path, -3))=='png'):
          $imageTmp     = imagecreatefrompng($source_path);
    endif;

    imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
    output_img($imageResized, image_type_to_mime_type(getimagesize($source_path)), $destination_path);
}






function create_medium($type, $image){
    
	// Get the original geometry and calculate scales
	echo $source_path=DIR_FS_SITE.'upload/photo/'.$type.'/large/'.$image;
	echo $destination_path=DIR_FS_SITE.'upload/photo/'.$type.'/medium/'.$image;
	list($width, $height) = getimagesize($source_path);
    $xscale=$width/MEDIUM_WIDTH;
    $yscale=$height/MEDIUM_HEIGHT;
    
    // Recalculate new size with default ratio
    if ($yscale>$xscale){
        $new_width = round($width * (1/$yscale));
        $new_height = round($height * (1/$yscale));
    }
    else {
        $new_width = round($width * (1/$xscale));
        $new_height = round($height * (1/$xscale));
    }

    #check image format and create image
    if(strtolower(substr($source_path, -3))=='jpg'):
         $imageTmp     = imagecreatefromjpeg ($source_path);
    elseif(strtolower(substr($source_path, -3))=='gif'):
         $imageTmp     = imagecreatefromgif($source_path);
    elseif(strtolower(substr($source_path, -3))=='png'):
          $imageTmp     = imagecreatefrompng($source_path);
    endif;

    // Resize the original image & output
    $imageResized = imagecreatetruecolor($new_width, $new_height);
    # $imageTmp     = imagecreatefromjpeg ($source_path);
    imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
    output_img($imageResized, image_type_to_mime_type(getimagesize($source_path)), $destination_path);
}



function get_thumb($type, $image)
{
	echo DIR_WS_SITE.'upload/photo/'.$type.'/thumb/'.$image;
}

function get_resized($type, $image, $folderName='thumb'){
	echo DIR_WS_SITE.'upload/photo/'.$type.'/'.$folderName.'/'.$image;
}

/*function get_medium($type, $image)
{
	echo DIR_WS_SITE.'upload/photo/'.$type.'/medium/'.$image;
}*/
function get_medium($type, $image, $rtype=false)
{ 
	if(file_exists(DIR_FS_SITE.'upload/photo/'.$type.'/medium/'.$image) && $image):
                if($rtype):
                    return DIR_WS_SITE.'upload/photo/'.$type.'/medium/'.$image;
                else:
                    echo   DIR_WS_SITE.'upload/photo/'.$type.'/medium/'.$image;
                endif;
	else:
                if($rtype):
                    return DIR_WS_SITE_GRAPHIC.'noimage.jpg';
                else:
                    echo   DIR_WS_SITE_GRAPHIC.'noimage.jpg';
                endif;
	endif;
}

function get_medium_img_tag($type, $image, $rtype=false){
    if($rtype)
        return '<img src="'.get_medium($type, $image, true).'" border="0" alt="'.$image.'">';
    else
        echo '<img src="'.get_medium($type, $image, true).'" border="0" alt="'.$image.'">';

}




function upload_photo($type, $file_name,$rand)
{	$admin_user=new admin_session();
	global $conf_allowed_photo_mime_type;
	if($file_name['error']):
		return false;
	endif;
	$image_name= make_image_name($file_name['name'], $rand); 
	
	if(in_array($file_name['type'], $conf_allowed_photo_mime_type)):
		if(move_uploaded_file($file_name['tmp_name'], DIR_FS_SITE.'upload/photo/'.$type.'/large/'.$image_name)):
			if(file_exists(DIR_FS_SITE.'upload/photo/'.$type.'/thumb/'.$image_name)):
				$admin_user->set_pass_msg('Image already exists with same name.Please select another image .');	
				return false;
			else:
				create_resized_for_module($type, $image_name);
				//create_thumb($type, $image_name);
				//create_medium($type, $image_name);
				return true;
			endif;
		else:
			return false;
		endif;
	endif;
	return false;
}


function get_large($type, $image)
{
	echo DIR_WS_SITE.'upload/photo/'.$type.'/large/'.$image;
}
function get_custom_image($type, $folder, $image)
{
	echo DIR_WS_SITE.'upload/photo/'.$type.'/'.$folder.'/'.$image;
}

function get_control_icon($name)
{
	return '<img src="'.DIR_WS_SITE.ADMIN_FOLDER.'/images/table/'.$name.'.png" border="none" >';
      
}

function make_image_name($name, $id)
{
	$file_name_parts=explode('.', $name);
	$file_name_parts['0'].=$id;
	return $file_name_parts['0'].'.'.$file_name_parts['1'];
}

function get_facebook_icon($module,$id)
{
if(defined('FDP') && FDP==1){
return '<a href="'.DIR_WS_SITE.ADMIN_FOLDER.'/facebook/post.php?mod='.$module.'&id='.$id.'" class="tipTop fancybox" style="width:499;height:600px;" title="Click Here to Post a Message in Facebook Account"><img src="'.DIR_WS_SITE.ADMIN_FOLDER.'/images/table/facebook.png" border="none" ></a>';
}
}
function get_twitter_icon($module,$msg)
{
if(defined('TDP') && TDP==1){
return '<a href="'.DIR_WS_SITE.ADMIN_FOLDER.'/twitter/post.php?mod='.$module.'&msg='.$msg.'" class="tipTop fancybox" title="Click Here to Post a Message in Twitter Account"><img src="'.DIR_WS_SITE.ADMIN_FOLDER.'/images/table/twitter.png" border="none" >';
}
}


 /*
         * type = module
         * image = image name
         * size= thumb,medium,large etc
         * rtype = return image path or not
         */ 
          function get_image($type, $size, $image, $rtype=false)
            {
                    if(file_exists(DIR_FS_SITE.'upload/photo/'.$type.'/'.$size.'/'.$image) && $image):
                            if($rtype):
                                return DIR_WS_SITE.'upload/photo/'.$type.'/'.$size.'/'.$image;
                            else:
                                echo   DIR_WS_SITE.'upload/photo/'.$type.'/'.$size.'/'.$image;
                            endif;
                    else:
                            if($rtype):
                                return DIR_WS_SITE_GRAPHIC.'noimage.jpg';
                            else:
                                echo   DIR_WS_SITE_GRAPHIC.'noimage.jpg';
                            endif;
                    endif;
            }  
            
            
          function delete_if_image_exists($type, $size, $image)
            {
                    if(file_exists(DIR_FS_SITE.'upload/photo/'.$type.'/'.$size.'/'.$image)):
                            unlink(DIR_FS_SITE.'upload/photo/'.$type.'/'.$size.'/'.$image);
                    endif;
            } 
            
             function DeleteImagesFromAllFolders($type,$image)
            {
                  global $conf_resizedCriteria;
                  /*delete large image if exists*/
                  if(file_exists(DIR_FS_SITE.'upload/photo/'.$type.'/large/'.$image)):
                     @unlink(DIR_FS_SITE.'upload/photo/'.$type.'/large/'.$image);
                  endif;
                  /*delete resized image if exists*/
                  if(file_exists(DIR_FS_SITE.'upload/photo/'.$type.'/resized/'.$image)):
                    @unlink(DIR_FS_SITE.'upload/photo/'.$type.'/resized/'.$image);
                  endif;
                  /*delete from all other folders*/
                  foreach($conf_resizedCriteria[$type] as $key=>$value):
                        if(file_exists(DIR_FS_SITE.'upload/photo/'.$type.'/'.$key.'/'.$image)):
                                @unlink(DIR_FS_SITE.'upload/photo/'.$type.'/'.$key.'/'.$image);
                        endif;
                  endforeach;       
            } 
            
            
            
            /*Crop Image*/
            function crop_photo($type, $size, $image,$crop_width,$crop_height,$crop_x,$crop_y,$new_width,$new_height){

                    if(file_exists(DIR_FS_SITE.'upload/photo/'.$type.'/'.$size.'/'.$image)):

                                    create_Resizedd($type,$size,$image,$new_width,$new_height);

                                   /*Get the original geometry and calculate scales*/

                                    $source_path=DIR_FS_SITE.'upload/photo/'.$type.'/resized/'.$image;
                                    $destination_path=DIR_FS_SITE.'upload/photo/'.$type.'/'.$size.'/'.$image;

                                    list($width, $height, $itype) = getimagesize($source_path);
                                   /*Resize the original image & output*/	 

                                    /*check image format and create image*/
                                   if(strtolower(substr($source_path, -3))=='jpg'):
                                         $imageTmp     = imagecreatefromjpeg ($source_path);
                                    elseif(strtolower(substr($source_path, -3))=='gif'):
                                         $imageTmp     = imagecreatefromgif($source_path);
                                    elseif(strtolower(substr($source_path, -3))=='png'):
                                          $imageTmp     = imagecreatefrompng($source_path);
                                    endif;
                                    $imageResized = imagecreatetruecolor($crop_width, $crop_height);
                                    imagecopyresized($imageResized, $imageTmp, 0, 0, $crop_x, $crop_y, $crop_width, $crop_height, $crop_width, $crop_height);
                                    output_img($imageResized, image_type_to_mime_type($itype), $destination_path); 
                                    
                                    return true;
                   else:

                                    return false;
                   endif;

            }
            
           function create_Resizedd($type, $size, $image, $w, $h){
    
                    // Get the original geometry and calculate scales
                  
                    $source_path=DIR_FS_SITE.'upload/photo/'.$type.'/large/'.$image;
                    $destination_path=DIR_FS_SITE.'upload/photo/'.$type.'/resized/'.$image;

                    list($width, $height, $itype, $attr) = getimagesize($source_path);
                    $new_width=$w;
                    $new_height=$h;

                #check image format and create image
                if(strtolower(substr($source_path, -3))=='jpg'):
                     $imageTmp     = imagecreatefromjpeg ($source_path);
                elseif(strtolower(substr($source_path, -3))=='gif'):
                     $imageTmp     = imagecreatefromgif($source_path);
                elseif(strtolower(substr($source_path, -3))=='png'):
                      $imageTmp     = imagecreatefrompng($source_path);
                endif;

                // Resize the original image & output
                $imageResized = imagecreatetruecolor($new_width, $new_height);
                # $imageTmp     = imagecreatefromjpeg ($source_path);
                imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                output_img($imageResized, image_type_to_mime_type($itype), $destination_path);

            }

        function output_img($rs, $mime, $path)
        {
                switch ($mime)
                {
                        case 'image/jpeg':
                        case 'image/jpg':
                        case 'image/pjpeg':
                        case 'image/pjpg':
                                        imagejpeg($rs, $path, 100);
                                        break;
                        case 'image/gif':
                                        imagegif($rs, $path, 100);
                                        break;
                        case 'image/png':
                                        imagepng($rs, $path, 9);
                                        break;
                        default:
                                        imagejpeg($rs, $path, 100);
                }
        }
           
?>